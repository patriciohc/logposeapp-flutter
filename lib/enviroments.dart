const bool isProduction = true;

const _devConfig = {
  'logposeapi': 'https://ws-neec.herokuapp.com/api'
  // 'logposeapi': 'http://localhost:8088/api'
};

const _productionConfig = {
  // 'logposeapi': 'https://voyage-madame-11841.herokuapp.com/api'
  'logposeapi': 'https://ws.logposeapp.com/api'
};

final enviroment = isProduction ? _productionConfig : _devConfig;
