import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logposeapp/blocs/account/account.dart';
import 'package:logposeapp/blocs/account/account_bloc.dart';

import 'package:logposeapp/blocs/categorias/categoria_bloc.dart';
import 'package:logposeapp/blocs/categorias/categoria_state.dart';
import 'package:logposeapp/blocs/comprador/comprador_bloc.dart';
import 'package:logposeapp/blocs/sucursal_list/sucursal_list.dart';
import 'package:logposeapp/models/categoria_model.dart';
import 'package:logposeapp/utils/dialogs.dart';
import 'package:logposeapp/widgets/cart_widget.dart';
import 'package:logposeapp/widgets/favorites_widget.dart';
import 'package:logposeapp/widgets/menu_widget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePage createState() {
    return _HomePage();
  }
}

class _HomePage extends State<HomePage> {
  String _selectedCategoria = '0';
  double _proximidad = 0.001;

  @override
  Widget build(BuildContext superContext) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text('Incio'),
        actions: <Widget>[
          CartWidget(),
        ],
      ),
      drawer: MenuWidget(),
      body: Column(
        children: <Widget>[
          Expanded(
            child: _createSearch(superContext),
          ),
          Container(
            decoration: BoxDecoration(
              color: Color(0xff455A64),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 1.0),
                  blurRadius: 10.0,
                ),
              ],
            ),
            padding: EdgeInsets.only(top: 15.0),
            height: 170.0,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 20.0),
                  child: Text(
                    'Accesos directos',
                    style: TextStyle(color: Colors.white, fontSize: 16.0),
                  ),
                ),
                BlocBuilder<AccountBloc, AccountState>(
                  builder: (context, state) {
                    if (state is AccountLoadSuccess) {
                      return _footer(context);
                    } else {
                      return _footerNoData();
                    }
                  },
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _footerNoData() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 30.0),
          Text(
            'No se ha iniciado sesion',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.0,
            ),
          ),
        ],
      ),
    );
  }

  Widget _createSearch(context) {
    return Container(
      width: 320.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 25.0),
          Text(
            'Seleccione una categoria',
            style: TextStyle(color: Color(0xff757575)),
          ),
          SizedBox(height: 5.0),
          BlocBuilder<CategoriaBloc, CategoriaState>(
            builder: (context, state) => _createDropDownList(state),
          ),
          SizedBox(height: 20.0),
          Text(
            'Que tan cerca quieres encontrar lo que buscas',
            style: TextStyle(color: Color(0xff757575)),
          ),
          SizedBox(height: 20.0),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Cerca',
                  style: TextStyle(color: Colors.black),
                ),
                Text(
                  'Lejos',
                  style: TextStyle(color: Colors.black),
                ),
              ]),
          Slider(
            activeColor: Colors.teal,
            inactiveColor: Color(0x2f009688),
            value: _proximidad,
            label: 'Proximidad',
            min: 0.001,
            max: 0.3,
            onChanged: (valor) {
              setState(() {
                _proximidad = valor;
              });
            },
          ),
          SizedBox(height: 25.0),
          _createBtnSearch(),
          SizedBox(height: 20.0),
          _createBtnScan(context),
        ],
      ),
    );
  }

  Widget _createBtnSearch() {
    return RaisedButton(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15.0),
        child: Center(child: Text('Buscar')),
        width: 300.0,
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 0.0,
      color: Colors.teal,
      textColor: Colors.white,
      onPressed: () => searhSucursales(context),
    );
  }

  Widget _createBtnScan(context) {
    return RaisedButton(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15.0),
        child: Center(child: Text('Escanera tarjeta de presentación')),
        width: 300.0,
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 0.0,
      color: Colors.teal,
      textColor: Colors.white,
      onPressed: () {
        onScanQr(context);
      },
    );
  }

  searhSucursales(context) {
    final selectedCategoria =
        _selectedCategoria != '0' ? _selectedCategoria : null;
    BlocProvider.of<SucursalListBloc>(context)
        .add(SucursalListFindEvent(_proximidad, selectedCategoria));
    Navigator.pushNamed(context, 'list-sucursales');
  }

  Widget _createDropDownList(state) {
    if (state is CategoriaIsLoading) {
      return Center(child: CircularProgressIndicator());
    }

    List<CategoriaModel> listCat = [];

    if (state is CategoriaLoadSuccess) {
      listCat = state.categorias;
    }

    return DropdownButton(
        isExpanded: true,
        value: _selectedCategoria,
        items: getOpcionesDropdown(listCat),
        onChanged: (opt) {
          setState(() {
            _selectedCategoria = opt;
          });
        });
  }

  List<DropdownMenuItem<String>> getOpcionesDropdown(
      List<CategoriaModel> categories) {
    List<DropdownMenuItem<String>> lista = new List();

    lista.add(DropdownMenuItem(
      child: Text('Seleccione una categoria'),
      value: '0',
    ));

    categories.forEach((cat) {
      lista.add(DropdownMenuItem(
        child: Text(cat.dsc),
        value: cat.id,
      ));
    });
    return lista;
  }

  Widget _footer(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 5.0),
          BlocBuilder<CompradorBloc, CompradorState>(
            builder: (BuildContext context, state) {
              if (state is CompradorLoadSuccess) {
                return FavoritesWidget(favorites: state.comprador.favoritos);
              } else if (state is CompradorIsLoading) {
                return Center(child: CircularProgressIndicator());
              } else {
                return Center(
                  child: Text(
                    'No se han agregado accesos directos',
                    style: TextStyle(color: Colors.white),
                  ),
                );
              }
            },
          ),
        ],
      ),
    );
  }

  Future onScanQr(context) async {
    try {
      final barcode = await BarcodeScanner.scan();
      print(barcode.rawContent);
      Navigator.pushNamed(context, 'sucursal', arguments: barcode.rawContent);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        showMessage(context, 'Se requieren permisos para acceder a la camara');
      } else {
        showMessage(context, 'Occuio un error al intertar leer el codigo qr');
      }
    }
  }
}
