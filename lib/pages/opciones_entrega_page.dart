import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logposeapp/blocs/shoping_cart/shoping_cart_bloc.dart';
import 'package:logposeapp/models/cart_model.dart';
import 'package:logposeapp/models/sucursal_model.dart';
import 'package:logposeapp/providers/sucursal_provider.dart';
import 'package:logposeapp/utils/dialogs.dart';

class OpcionesEntregaPage extends StatefulWidget {
  @override
  _OpcionesEntregaPage createState() {
    return _OpcionesEntregaPage();
  }
}

class _OpcionesEntregaPage extends State<OpcionesEntregaPage> {
  final sucursalProvider = SucursalProvider();
  TipoEntrega _tipoEntrega = TipoEntrega.MOSTRADOR;
  TipoHoraEntrega _tipoHoraEntrega = TipoHoraEntrega.LO_ANTES_POSIBLE;
  DateTime _diaEntrega;
  TimeOfDay _horaEntrega;
  bool isLoadingShoping = false;

  set tipoEntrega(value) {
    if (_tipoEntrega == null) _tipoEntrega = value;
  }

  set tipoHoraEntrega(value) {
    if (_tipoHoraEntrega == null) _tipoHoraEntrega = value;
  }

  set diaEntrega(value) {
    if (_diaEntrega == null) _diaEntrega = value;
  }

  set horaEntrega(value) {
    if (_horaEntrega == null) _horaEntrega = value;
  }

  @override
  Widget build(BuildContext context) {
    final CartModel cart = ModalRoute.of(context).settings.arguments;

    if (cart.informacionEntrega != null) {
      tipoEntrega = cart.informacionEntrega.tipoEntrega;
      tipoHoraEntrega = cart.informacionEntrega.tipoHoraEntrega;
      diaEntrega = cart.informacionEntrega.diaEntrega;
      horaEntrega = cart.informacionEntrega.horaEntrega;
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text('Opciones de entrega'),
      ),
      body: BlocBuilder<ShopingCartBloc, ShopingCartState>(
        builder: (context, state) {
          if (state is ShopingCartIsLoading) {
            SchedulerBinding.instance.addPostFrameCallback((_) {
              if (!isLoadingShoping) {
                showLoading(context, 'Enviando pedido...');
                isLoadingShoping = true;
              }
            });
          } else if (state is SendPedidoSuccess) {
            SchedulerBinding.instance.addPostFrameCallback((_) {
              Navigator.popAndPushNamed(context, 'home');
              showMessage(context, 'El pedido fue enviado');
              isLoadingShoping = false;
            });
          } else if (state is SendPedidoFailed) {
            SchedulerBinding.instance.addPostFrameCallback((_) {
              showMessage(
                  context, 'No se pudo enviar el pedido, intente mas tarde');
              isLoadingShoping = false;
            });
          }
          return Center(child: _createFom(context, cart));
        },
      ),
    );
  }

  Widget _createFom(context, CartModel cart) {
    final sucursal = cart.unidad;

    return Column(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(30),
            child: _createForm(sucursal),
          ),
        ),
        _createButtons(cart),
      ],
    );
  }

  _createForm(SucursalModel sucursal) {
    final text = sucursal.servicioDomicilio ? 'cuenta' : 'no cuenta';
    final inicio = sucursal.horaApertura;
    final fin = sucursal.horaCierre;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Forma de entrega',
          style: TextStyle(color: Color(0xff757575)),
        ),
        DropdownButton(
          isExpanded: true,
          value: _tipoEntrega,
          items: _getOptionsFormaEntrega(sucursal.servicioDomicilio),
          onChanged: (opt) {
            setState(() {
              _tipoEntrega = opt;
            });
          },
        ),
        SizedBox(height: 20.0),
        Text(
          'Programar entrega',
          style: TextStyle(color: Color(0xff757575)),
        ),
        DropdownButton(
            isExpanded: true,
            value: _tipoHoraEntrega,
            items: [
              DropdownMenuItem(
                child: Text('Entregar lo antes posible'),
                value: TipoHoraEntrega.LO_ANTES_POSIBLE,
              ),
              DropdownMenuItem(
                child: Text('Programar entrega'),
                value: TipoHoraEntrega.HORA_PROGRAMADA,
              ),
            ],
            onChanged: (opt) {
              setState(() {
                _tipoHoraEntrega = opt;
              });
            }),
        SizedBox(height: 25.0),
        _getInputDatePicker(_tipoHoraEntrega),
        SizedBox(height: 20.0),
        Text('Horario: $inicio a $fin'),
        Text('Esta sucursal $text con servicio a domicilio'),
        Text('Proximamente mas formas de pago'),
      ],
    );
  }

  _getOptionsFormaEntrega(bool servicioDomicilio) {
    List<DropdownMenuItem<TipoEntrega>> items = [
      DropdownMenuItem(
        child: Text('Entrega en mostrador'),
        value: TipoEntrega.MOSTRADOR,
      ),
    ];

    if (servicioDomicilio) {
      items.add(
        DropdownMenuItem(
          child: Text('Entrega en domicilio'),
          value: TipoEntrega.DOMICILIO,
        ),
      );
    }
    return items;
  }

  Future<DateTime> _getDate() {
    DateTime now = DateTime.now();
    return showDatePicker(
      context: context,
      initialDate: now,
      firstDate: now,
      lastDate: new DateTime(now.year, now.month, now.day + 10),
      initialEntryMode: DatePickerEntryMode.calendar,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light(),
          child: child,
        );
      },
    );
  }

  Future<TimeOfDay> _getTime() {
    TimeOfDay now = TimeOfDay.now();
    return showTimePicker(
      initialTime: new TimeOfDay(hour: now.hour + 1, minute: now.minute),
      context: context,
    );
  }

  Widget _getInputDatePicker(TipoHoraEntrega tipoHoraEntrega) {
    String dateTime = buildDateTimeString();
    String text = dateTime != null ? dateTime : 'Programar entrega';

    return GestureDetector(
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.teal,
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        padding: EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(width: 6.0),
            Text(
              text,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
                color: Colors.white,
              ),
            ),
            Icon(
              Icons.calendar_today,
              color: Colors.white,
            ),
          ],
        ),
      ),
      onTap: () async {
        if (tipoHoraEntrega == TipoHoraEntrega.HORA_PROGRAMADA) {
          _diaEntrega = await _getDate();
          if (_diaEntrega != null) {
            _horaEntrega = await _getTime();
            setState(() {});
          }
        } else {
          showMessage(context, 'Eliga la opcion programar entrega');
        }
      },
    );
  }

  _continuar(CartModel cart, bool isActivo) {
    if (_addInfoEntrega(cart, isActivo)) {
      Navigator.pushNamed(context, 'ubicacion', arguments: cart);
    }
  }

  bool _addInfoEntrega(CartModel cart, bool isActivo) {
    final sucursal = cart.unidad;
    final apertura = _getTimeOfDay(sucursal.horaApertura);
    final cierre = _getTimeOfDay(sucursal.horaCierre);
    final now = TimeOfDay.now();
    if (_tipoHoraEntrega == TipoHoraEntrega.HORA_PROGRAMADA &&
        (_diaEntrega == null || _horaEntrega == null)) {
      showMessage(context, 'Indica la hora de entrega');
      return false;
    } else if (_tipoHoraEntrega == TipoHoraEntrega.HORA_PROGRAMADA) {
      if (_diaEntrega.day == DateTime.now().day) {
        if (!isActivo) {
          showMessage(context,
              'En este momento esta sucursal no recibe solicitudes en linea');
          return false;
        }
        if (_horaEntrega.hour <= now.hour) {
          showMessage(context,
              'Programa una entrega con una hora posterior a la actual');
          return false;
        } else if (_horaEntrega.hour == now.hour + 1 &&
            _horaEntrega.minute < now.minute - 5) {
          showMessage(context,
              'Programa una entrega con una hora posterior a la actual');
          return false;
        }
      }
      if (_timeToDouble(_horaEntrega) < _timeToDouble(apertura) ||
          _timeToDouble(_horaEntrega) > _timeToDouble(cierre)) {
        showMessage(context, 'Programa una entrega dentro del horario');
        return false;
      }
    } else if (_tipoHoraEntrega == TipoHoraEntrega.LO_ANTES_POSIBLE) {
      if (!isActivo) {
        showMessage(context,
            'En este momento esta sucursal no recibe solicitudes en linea');
        return false;
      }

      if (_timeToDouble(now) < _timeToDouble(apertura) ||
          _timeToDouble(now) > _timeToDouble(cierre)) {
        showMessage(context, 'La sucursal es fuera de servicio');
        return false;
      }
    }

    final infoEntrega = new InformacionEntrega();
    infoEntrega.diaEntrega = _diaEntrega;
    infoEntrega.horaEntrega = _horaEntrega;
    infoEntrega.tipoEntrega = _tipoEntrega;
    infoEntrega.tipoHoraEntrega = _tipoHoraEntrega;
    BlocProvider.of<ShopingCartBloc>(context).add(
      AddInfoEntrega(infoEntrega, sucursal),
    );
    return true;
  }

  TimeOfDay _getTimeOfDay(String time) {
    final parts = time.split(':');
    return TimeOfDay(hour: int.parse(parts[0]), minute: int.parse(parts[1]));
  }

  double _timeToDouble(TimeOfDay myTime) => myTime.hour + myTime.minute / 60.0;

  String buildDateTimeString() {
    if (_diaEntrega != null && _horaEntrega != null) {
      final int year = _diaEntrega.year;
      final int month = _diaEntrega.month;
      final int day = _diaEntrega.day;
      final int hour = _horaEntrega.hour;
      final int minute = _horaEntrega.minute;
      return '$year-$month-$day $hour:$minute';
    } else {
      return null;
    }
  }

  Widget _createButtons(CartModel cart) {
    String buttonText;
    if (_tipoEntrega == TipoEntrega.DOMICILIO) {
      buttonText = 'Continuar';
    } else {
      buttonText = 'Confirmar pedido';
    }

    final total = cart.total;

    return FutureBuilder(
      future: sucursalProvider.isActive(cart.unidad.id),
      builder: (context, AsyncSnapshot<bool> snapshot) {
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        }
        final isActivo = snapshot.data;
        return Container(
          color: Color(0xff455A64),
          width: double.infinity,
          padding: EdgeInsets.all(20.0),
          child: Column(children: <Widget>[
            Text(
              'Total a pagar: \$$total',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
                color: Colors.white,
              ),
            ),
            SizedBox(height: 10.0),
            RaisedButton(
              color: Colors.teal,
              child: Text(
                buttonText,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                if (_tipoEntrega == TipoEntrega.DOMICILIO) {
                  _continuar(cart, isActivo);
                } else {
                  _confirmarPedido(cart, isActivo);
                }
              },
            )
          ]),
        );
      },
    );
  }

  void _confirmarPedido(CartModel cart, bool isActivo) async {
    if (_addInfoEntrega(cart, isActivo)) {
      final sucursal = cart.unidad;

      BlocProvider.of<ShopingCartBloc>(context).add(
        ConfirmarPedido(sucursal),
      );
    }
  }
}
