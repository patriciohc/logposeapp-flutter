import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:logposeapp/blocs/shoping_cart/shoping_cart_bloc.dart';
import 'package:logposeapp/models/cart_model.dart';
import 'package:logposeapp/utils/dialogs.dart';

class UbicacionPage extends StatefulWidget {
  @override
  _UbicacionPage createState() {
    return _UbicacionPage();
  }
}

class _UbicacionPage extends State<UbicacionPage> {
  bool _loadingCurrentPosition;
  Position _currentPosition;
  GoogleMapController _mapController;
  double marketPosX;
  double marketPoxY;
  bool isLoadingShoping = false;

  _UbicacionPage() {
    super.initState();
    getCurrentPage();
  }

  @override
  Widget build(BuildContext context) {
    final CartModel cart = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text('Ubicacion'),
      ),
      body: BlocBuilder<ShopingCartBloc, ShopingCartState>(
        builder: (context, state) {
          if (state is ShopingCartIsLoading) {
            SchedulerBinding.instance.addPostFrameCallback((_) {
              if (!isLoadingShoping) {
                showLoading(context, 'Enviando pedido...');
                isLoadingShoping = true;
              }
            });
          } else if (state is SendPedidoSuccess) {
            SchedulerBinding.instance.addPostFrameCallback((_) {
              showMessage(context, 'El pedido se fue enviado');
              isLoadingShoping = false;
              Navigator.popAndPushNamed(context, 'home');
            });
          } else if (state is SendPedidoFailed) {
            SchedulerBinding.instance.addPostFrameCallback((_) {
              showMessage(
                  context, 'No se pudo enviar el pedido, intente mas tarde');
              isLoadingShoping = false;
            });
          }
          return Column(
            children: <Widget>[
              _createMapa(context),
              _createButtons(cart),
            ],
          );
        },
      ),
    );
  }

  Widget _createMapa(context) {
    if (_loadingCurrentPosition) {
      return Expanded(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    CameraPosition _initialPosition = CameraPosition(
      target: LatLng(19.39060, -99.283697),
      zoom: 7,
    );
    Completer<GoogleMapController> _controller = Completer();

    if (_currentPosition != null) {
      _initialPosition = CameraPosition(
        target: LatLng(_currentPosition.latitude, _currentPosition.longitude),
        zoom: 16,
      );
    }

    void _onMapCreated(GoogleMapController controller) {
      _mapController = controller;
      _controller.complete(controller);
    }

    return Expanded(
      child: Stack(children: <Widget>[
        GoogleMap(
          myLocationEnabled: true,
          onMapCreated: _onMapCreated,
          initialCameraPosition: _initialPosition,
        ),
        _createMarket(context),
      ]),
    );
  }

  Widget _createButtons(CartModel cart) {
    final total = cart.total;

    return Container(
      color: Color(0xff455A64),
      width: double.infinity,
      padding: EdgeInsets.all(20.0),
      child: Column(children: <Widget>[
        Text(
          'Total a pagar: \$2$total',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
            color: Colors.white,
          ),
        ),
        SizedBox(height: 10.0),
        RaisedButton(
          color: Colors.teal,
          child: Text(
            'Confirmar pedido',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          onPressed: () {
            _confirmarPedido(cart.unidad);
          },
        )
      ]),
    );
  }

  void _confirmarPedido(sucursal) async {
    if (_mapController != null) {
      ScreenCoordinate screenCoordinate = ScreenCoordinate(
        x: marketPosX.round(),
        y: marketPoxY.round(),
      );
      final latLng = await _mapController.getLatLng(screenCoordinate);
      BlocProvider.of<ShopingCartBloc>(context).add(
        AddDireccionEntrega(latLng.latitude, latLng.longitude, sucursal),
      );

      BlocProvider.of<ShopingCartBloc>(context).add(
        ConfirmarPedido(sucursal),
      );
      Navigator.pushNamed(context, 'home');
    } else {
      showMessage(context, 'No se ha iniciado el mapa correctamente');
    }
  }

  Widget _createMarket(context) {
    double mapWidth = MediaQuery.of(context).size.width;
    double mapHeight = MediaQuery.of(context).size.height - 215;
    double iconSize = 40.0;

    marketPosX = (mapHeight - iconSize) / 2;
    marketPoxY = (mapWidth - iconSize) / 2;

    return Positioned(
      top: marketPosX,
      right: marketPoxY,
      child: new Icon(Icons.person_pin_circle, size: iconSize),
    );
  }

  getCurrentPage() async {
    _loadingCurrentPosition = true;
    try {
      _currentPosition = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.medium)
          .timeout(Duration(seconds: 5));
      _loadingCurrentPosition = false;
      setState(() {});
    } catch (error) {
      _loadingCurrentPosition = false;
      _currentPosition = null;
      setState(() {});
    }
  }
}
