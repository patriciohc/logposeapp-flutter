import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logposeapp/blocs/account/account_bloc.dart';
import 'package:logposeapp/blocs/account/account_event.dart';
import 'package:logposeapp/blocs/account/account_state.dart';
import 'package:logposeapp/utils/forms_validations.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

bool dialogIsVisible = false;

class LoginPage extends StatelessWidget {
  final _userCtrl = TextEditingController();
  final _passwordCtrl = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AccountBloc, AccountState>(builder: (context, state) {
      if (state is LoginFailed) {
        SchedulerBinding.instance.addPostFrameCallback((_) {
          if (!dialogIsVisible) {
            dialogIsVisible = true;
            _showMyDialog(context, msg: state.message);
          }
        });
      } else if (state is HideError) {
        SchedulerBinding.instance.addPostFrameCallback((_) {
          if (dialogIsVisible) {
            dialogIsVisible = false;
            Navigator.pop(context);
          }
        });
      } else if (state is AccountLoadSuccess) {
        SchedulerBinding.instance.addPostFrameCallback((_) {
          Navigator.popAndPushNamed(context, 'home');
        });
      }

      return _createMainContent(context, state);
    });
  }

  Widget _createMainContent(context, state) {
    final isLoading =
        (state is LoggingIn || state is AccountLoadInProgress) ? true : false;
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: isLoading,
        child: Stack(
          children: <Widget>[
            _createFondo(context),
            _loginForm(context, state),
          ],
        ),
      ),
    );
  }

  Widget _loginForm(BuildContext context, state) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 50.0,
            ),
          ),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 15.0),
            padding: EdgeInsets.symmetric(vertical: 40.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black26,
                    blurRadius: 3.0,
                    offset: Offset(0.0, 5.0),
                    spreadRadius: 3.0)
              ],
            ),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Center(
                    child: Image(image: AssetImage('assets/img/logo.png')),
                  ),
                  SizedBox(height: 20.0),
                  _crearEmail(),
                  SizedBox(height: 30.0),
                  _crearPassword(),
                  SizedBox(height: 30.0),
                  _crearBoton(context, state)
                ],
              ),
            ),
          ),
          _createFootText(context),
          SizedBox(height: 50.0)
        ],
      ),
    );
  }

  Widget _createFootText(context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.pushNamed(context, 'reset-pass');
              },
              child: Text(
                '¿Olvido la contraseña?',
                style: TextStyle(color: Color(0xff757575)),
              ),
            ),
            FlatButton(
              onPressed: () => Navigator.pushNamed(context, 'signup'),
              child: Text(
                'Registrarse',
                style: TextStyle(color: Color(0xff757575)),
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget _crearEmail() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextFormField(
          controller: _userCtrl,
          validator: (value) => validate(value, [empty, email]),
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
              icon: Icon(Icons.alternate_email, color: Colors.teal),
              hintText: 'ejemplo@correo.com',
              labelText: 'Correo electrónico'),
        ));
  }

  Widget _crearPassword() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: TextFormField(
          controller: _passwordCtrl,
          validator: (value) => validate(value, [empty, min(3)]),
          obscureText: true,
          decoration: InputDecoration(
            icon: Icon(Icons.lock_outline, color: Colors.teal),
            labelText: 'Contraseña',
          )),
    );
  }

  Widget _crearBoton(context, state) {
    return Column(children: <Widget>[
      RaisedButton(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 15.0),
          child: Center(child: Text('Ingresar')),
          width: 220.0,
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        elevation: 0.0,
        color: Colors.teal,
        textColor: Colors.white,
        onPressed: () => _login(context),
      ),
      SizedBox(height: 10.0),
      RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 15.0),
            child: Center(child: Text('Saltar y continuar')),
            width: 220.0,
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          elevation: 0.0,
          color: Colors.teal,
          textColor: Colors.white,
          onPressed: () => Navigator.pushNamed(context, 'home'))
    ]);
  }

  Widget _createFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      color: Color(0xff607D8B),
      height: size.height * 0.35,
      width: double.infinity,
    );
  }

  _login(context) async {
    if (_formKey.currentState.validate()) {
      BlocProvider.of<AccountBloc>(context)
          .add(LogInEvent(_userCtrl.text, _passwordCtrl.text));
    }
  }

  Future<void> _showMyDialog(context,
      {String msg: 'El usuario o contraseña son incorrectos'}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(msg),
          actions: <Widget>[
            FlatButton(
              child: Text('Acpetar'),
              onPressed: () {
                BlocProvider.of<AccountBloc>(context).add(HideErrorEvent());
              },
            ),
          ],
        );
      },
    );
  }
}
