import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_image/network.dart';
import 'package:logposeapp/blocs/account/account.dart';
import 'package:logposeapp/blocs/account/account_bloc.dart';
import 'package:logposeapp/blocs/comprador/comprador_bloc.dart';
import 'package:logposeapp/blocs/products/products_bloc.dart';
import 'package:logposeapp/blocs/shoping_cart/shoping_cart_bloc.dart';
import 'package:logposeapp/blocs/sucursal/sucursal_bloc.dart';
import 'package:logposeapp/models/product_model.dart';
import 'package:logposeapp/models/sucursal_model.dart';
import 'package:logposeapp/search/productos_search.dart';
import 'package:logposeapp/utils/dialogs.dart';
import 'package:logposeapp/widgets/cart_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class SucursalPage extends StatelessWidget {
  String catSelected;

  @override
  Widget build(BuildContext context) {
    final sucursal = ModalRoute.of(context).settings.arguments;

    if (sucursal is SucursalModel) {
      return _createMainPage(context, sucursal);
    } else {
      BlocProvider.of<SucursalBloc>(context).add(
        SucursalFindEvent(sucursal),
      );
      return BlocBuilder<SucursalBloc, SucursalState>(
        builder: (context, state) {
          if (state is SucursalFindIsLoading) {
            return Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else if (state is SucursalFindSuccess) {
            return _createMainPage(context, state.sucursal);
          } else {
            return Scaffold(
              body: Center(
                child: Text('No se encontro la sucursal'),
              ),
            );
          }
        },
      );
    }
  }

  Widget _createMainPage(context, SucursalModel sucursal) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _createAppbar(context, sucursal),
          _createPresentation(context, sucursal),
          _createSilverGrid(context, sucursal),
        ],
      ),
      floatingActionButton: BlocBuilder<AccountBloc, AccountState>(
        builder: (context, state) {
          return FloatingActionButton(
            backgroundColor: Colors.teal,
            child: Icon(Icons.star),
            onPressed: () {
              _addFavoritos(context, sucursal, state);
            },
          );
        },
      ),
    );
  }

  Widget _createPresentation(context, SucursalModel sucursal) {
    final size = MediaQuery.of(context).size;

    return SliverList(
      delegate: SliverChildListDelegate([
        SizedBox(height: 10.0),
        ListTile(
            title: Text(sucursal.nombreEmpresa),
            subtitle: Row(
              children: <Widget>[
                Container(
                  width: size.width - 220,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(sucursal.direccion),
                        Text('Tel: ' + sucursal.telefono),
                        Text('Horario: ' +
                            sucursal.horaApertura +
                            ' - ' +
                            sucursal.horaCierre),
                      ]),
                ),
                IconButton(
                  onPressed: () {
                    _openMap(sucursal);
                  },
                  icon: Icon(Icons.directions),
                ),
                IconButton(
                  onPressed: () {
                    launch('tel://' + sucursal.telefono);
                  },
                  icon: Icon(Icons.call),
                )
              ],
            ),
            leading: Image(
              width: 60,
              height: 48,
              image: NetworkImageWithRetry(sucursal.logo),
            ),
            onTap: null),
        SizedBox(height: 10.0),
      ]),
    );
  }

  Widget _createSilverGrid(context, sucursal) {
    final size = MediaQuery.of(context).size;

    return BlocBuilder<ProductsBloc, ProductsState>(
      builder: (context, state) {
        if (state is ProductsInitial) {
          BlocProvider.of<ProductsBloc>(context)
              .add(ProductsLoadEvent(sucursal.id));
          return SliverList(
            delegate: SliverChildListDelegate([
              Container(
                  height: size.height - 400.0,
                  child: Center(child: CircularProgressIndicator()))
            ]),
          );
        } else if (state is ProductsLoadSuccess) {
          return SliverList(
            delegate: _createListItem(context, state.products, sucursal),
          );
        } else if (state is ProductsIsLoading) {
          return SliverList(
            delegate: SliverChildListDelegate([
              Container(
                  height: size.height - 400.0,
                  child: Center(child: CircularProgressIndicator()))
            ]),
          );
        } else {
          return SliverList(
            delegate: SliverChildListDelegate([
              Container(
                  height: size.height - 400.0,
                  child: Center(child: Text('No hay productos registrados')))
            ]),
          );
        }
      },
    );
  }

  Widget _createAppbar(BuildContext context, SucursalModel sucursal) {
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.blueGrey,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.filter_list),
          onPressed: () {
            _filterDialog(context, sucursal.categoriaProductos, sucursal.id);
          },
        ),
        IconButton(
          icon: Icon(Icons.search),
          onPressed: () {
            showSearch(
              context: context,
              delegate: ProductosSearch(sucursal, this.catSelected),
            );
          },
        ),
        CartWidget(),
      ],
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: false,
        title: Text(
          sucursal.nombreEmpresa,
          style: TextStyle(color: Colors.white, fontSize: 16.0),
        ),
        background: FadeInImage(
          image: NetworkImageWithRetry(sucursal.promocionalImg),
          placeholder: AssetImage('assets/img/loading.gif'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  SliverChildDelegate _createListItem(
    context,
    List<ProductModel> lista,
    SucursalModel sucursal,
  ) {
    final size = MediaQuery.of(context).size;
    return SliverChildBuilderDelegate(
      (BuildContext context, int index) {
        return Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: Column(
            children: <Widget>[
              ListTile(
                leading: FadeInImage(
                  image: NetworkImageWithRetry(lista[index].firstImage),
                  placeholder: AssetImage('assets/img/loading.gif'),
                  fit: BoxFit.cover,
                ),
                title: Text(lista[index].nombre),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: size.width - 200,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(lista[index].descripcion),
                          if (sucursal.hasSolicitudServicio)
                            Text('\$' + lista[index].precio.toString()),
                          if (!sucursal.hasSolicitudServicio)
                            Text('Precio no disponible')
                        ],
                      ),
                    ),
                    if (sucursal.hasSolicitudServicio)
                      IconButton(
                        color: Colors.red,
                        icon: Icon(Icons.add_shopping_cart),
                        onPressed: () {
                          BlocProvider.of<ShopingCartBloc>(context).add(
                              AddElementToCartEvent(lista[index], sucursal));
                        },
                      )
                  ],
                ),
              ),
            ],
          ),
          //
        );
      },
      childCount: lista.length,
    );
  }

  Future<void> _filterDialog(
    context,
    List<CategoriaProductos> categorias,
    String sucursalId,
  ) async {
    String _selectedCategoria = '0';

    final catSelected = await showDialog<String>(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
                title: const Text('Realizar busqueda'),
                content: Container(
                    height: 100.0,
                    child: Column(children: <Widget>[
                      DropdownButton(
                        isExpanded: true,
                        value: _selectedCategoria,
                        items: getOpcionesDropdown(categorias),
                        onChanged: (opt) {
                          _selectedCategoria = opt;
                          setState(() {});
                        },
                      )
                    ])),
                actions: <Widget>[
                  FlatButton(
                      onPressed: () {
                        if (_selectedCategoria == '0') {
                          Navigator.pop(context, null);
                        } else {
                          final selectedCategoria = _selectedCategoria != '0'
                              ? _selectedCategoria
                              : null;
                          Navigator.pop(context, selectedCategoria);
                        }
                      },
                      child: Text('Buscar')),
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                      child: Text('Cancelar'))
                ]);
          },
        );
      },
    );
    this.catSelected = catSelected;
    if (catSelected != null) {
      BlocProvider.of<ProductsBloc>(context)
          .add(ProductsLoadEvent(sucursalId, categoriaId: catSelected));
    }
  }

  List<DropdownMenuItem<String>> getOpcionesDropdown(
    List<CategoriaProductos> categorias,
  ) {
    List<DropdownMenuItem<String>> lista = new List();

    lista.add(
      DropdownMenuItem(
        child: Text('Seleccione una categoria'),
        value: '0',
      ),
    );

    categorias.forEach((cat) {
      lista.add(
        DropdownMenuItem(
          child: Text(cat.descripcion),
          value: cat.id,
        ),
      );
    });
    return lista;
  }

  _addFavoritos(context, sucursal, state) {
    if (state is AccountLoadSuccess) {
      BlocProvider.of<CompradorBloc>(context).add(
        CompradorAddFavoritosEvent(sucursal),
      );
    } else {
      showMessage(context, 'Inicie sesión para agregar favoritos');
    }
  }

  Future<void> _openMap(SucursalModel sucursal) async {
    double lat = sucursal.lat;
    double lng = sucursal.lng;
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$lat,$lng';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }
}
