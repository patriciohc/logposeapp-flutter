import 'package:flutter/material.dart';
import 'package:logposeapp/models/email_model.dart';
import 'package:logposeapp/providers/usuario_provider.dart';
import 'package:logposeapp/utils/dialogs.dart';
import 'package:logposeapp/utils/forms_validations.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class ResetPassPage extends StatefulWidget {
  @override
  _ResetPassPage createState() {
    return _ResetPassPage();
  }
}

class _ResetPassPage extends State<ResetPassPage> {
  final _usuarioProvider = UsuarioProvider();
  final _emailCrl = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  @override
  Widget build(BuildContext superContext) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text('Restablecer contraseña'),
      ),
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: Center(
          child: Container(
            width: 300.0,
            child: _createForm(),
          ),
        ),
      ),
    );
  }

  Widget _createForm() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Se te enviara un correo electrónico para restablecer tu contraseña',
            style: TextStyle(fontSize: 18.0),
          ),
          SizedBox(height: 30.0),
          TextFormField(
            keyboardType: TextInputType.emailAddress,
            controller: _emailCrl,
            decoration: InputDecoration(labelText: 'Correo electronico'),
            validator: (value) => validate(value, [empty, email]),
          ),
          SizedBox(height: 30.0),
          RaisedButton(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 15.0),
              child: Center(child: Text('Restablecer contraseña')),
              // width: 300.0,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            elevation: 0.0,
            color: Colors.teal,
            textColor: Colors.white,
            onPressed: () => resetPassword(context),
          ),
        ],
      ),
    );
  }

  void resetPassword(context) async {
    if (_formKey.currentState.validate()) {
      try {
        setState(() {
          _isLoading = true;
        });
        final email = EmailModel(_emailCrl.text);
        try {
          await _usuarioProvider.resetPassword(email);
          setState(() {
            _isLoading = false;
          });
        } catch (error) {
          showMessage(context, 'Ocurrio un error intenta mas tarde por favor');
          setState(() {
            _isLoading = false;
          });
        }

        Navigator.pushNamed(context, 'login');
      } catch (error) {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }
}
