import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logposeapp/blocs/pedidos/pedidos_bloc.dart';
import 'package:logposeapp/models/cart_model.dart';

class ListaPedidosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text('Lista de pedidos'),
      ),
      body: _createBody(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal,
        child: Icon(Icons.update),
        onPressed: () {
          BlocProvider.of<PedidosBloc>(context).add(PedidosGetEvent());
        },
      ),
    );
  }

  Widget _createBody() {
    return BlocBuilder<PedidosBloc, PedidosState>(
      builder: (context, state) {
        if (state is PedidosIsLoading) {
          return Center(child: CircularProgressIndicator());
        } else if (state is PedidosUpdate) {
          return _createList(context, state.compras);
        } else {
          return Center(
            child: Text('Ocurrio un error al cargar la lista de pedidos'),
          );
        }
      },
    );
  }

  Widget _createList(BuildContext context, List<CartModel> pedidos) {
    return ListView(
      shrinkWrap: true,
      children: pedidos.map((item) {
        print(item);
        return ListTile(
          leading: CircleAvatar(
            backgroundColor: _getColorStatus(item.estatus),
            child: Text(
              _getLetterStatus(item.estatus),
              style: TextStyle(color: Colors.white),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(item.unidad.nombreEmpresa),
              Text(item.noTicket),
            ],
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Total: \$' + item.total.toString()),
              Text('Fecha: ' + item.fecha.toLocal().toString()),
              Text(_getNameStatus(item.estatus))
            ],
          ),
          onTap: () {
            // Navigator.pushNamed(context, 'sucursal', arguments: item);
          },
        );
      }).toList(),
    );
  }

  String _getNameStatus(EstatusVenta estatus) {
    if (estatus == EstatusVenta.ESPERANDO) {
      // azul
      return 'Tu pedido esta en espera de aceptacion';
    } else if (estatus == EstatusVenta.ACEPTADO) {
      // amarillo
      return 'Tu pedido fue aceptado';
    } else if (estatus == EstatusVenta.EN_PROGRESO) {
      // verde
      return 'Tu pedido esta en proceso';
    } else if (estatus == EstatusVenta.FINALIZADO) {
      //
      return 'Pedido finalizado';
    } else if (estatus == EstatusVenta.CANCELADO) {
      // rojo
      return 'Tu pedido fue cancelado';
    } else {
      return '';
    }
  }

  Color _getColorStatus(EstatusVenta estatus) {
    if (estatus == EstatusVenta.ESPERANDO) {
      // azul
      return Colors.yellow;
    } else if (estatus == EstatusVenta.ACEPTADO) {
      // amarillo
      return Colors.blue;
    } else if (estatus == EstatusVenta.EN_PROGRESO) {
      // verde
      return Colors.green;
    } else if (estatus == EstatusVenta.FINALIZADO) {
      //
      return Colors.purple;
    } else if (estatus == EstatusVenta.CANCELADO) {
      // rojo
      return Colors.red;
    } else {
      return Colors.grey;
    }
  }

  String _getLetterStatus(estatus) {
    if (estatus == EstatusVenta.ESPERANDO) {
      return 'E';
    } else if (estatus == EstatusVenta.ACEPTADO) {
      return 'A';
    } else if (estatus == EstatusVenta.EN_PROGRESO) {
      return 'P';
    } else if (estatus == EstatusVenta.FINALIZADO) {
      return 'F';
    } else if (estatus == EstatusVenta.CANCELADO) {
      return 'C';
    } else {
      return 'N';
    }
  }
}
