import 'package:flutter/material.dart';
import 'package:logposeapp/models/usuario_model.dart';
import 'package:logposeapp/providers/usuario_provider.dart';
import 'package:logposeapp/utils/dialogs.dart';
import 'package:logposeapp/utils/forms_validations.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPage createState() {
    return _SignUpPage();
  }
}

class _SignUpPage extends State<SignUpPage> {
  final _usuarioProvider = UsuarioProvider();
  final _formKey = GlobalKey<FormState>();
  final _usr = UsuarioModel.initController();

  bool _isLoading = false;
  bool _passwordVisible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text('Registro de usuario'),
      ),
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Padding(
                  padding: EdgeInsets.all(22.0),
                  child: Container(height: 550.0, child: _createForm(context))),
            )
          ],
        ),
      ),
    );
  }

  Widget _createForm(context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          TextFormField(
              controller: _usr.nombreCrtl,
              decoration: InputDecoration(labelText: 'Nombre'),
              validator: (value) => validate(value, [empty, min(4), max(15)])),
          TextFormField(
            controller: _usr.apellidosCrtl,
            decoration: InputDecoration(labelText: 'Apellidos'),
            validator: (value) => validate(value, [empty, min(2), max(15)]),
          ),
          TextFormField(
            controller: _usr.correoElectronicoCrtl,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(labelText: 'Correo electronico'),
            validator: (value) => validate(value, [empty, email]),
          ),
          TextFormField(
            controller: _usr.telefonoCrtl,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(labelText: 'Telefono'),
            validator: (value) => validate(value, [empty, min(10), max(10)]),
          ),
          TextFormField(
            controller: _usr.passwordCrtl,
            obscureText: _passwordVisible,
            decoration: InputDecoration(
              labelText: 'Contraseña',
              suffixIcon: IconButton(
                icon: Icon(
                  _passwordVisible ? Icons.visibility : Icons.visibility_off,
                  color: Theme.of(context).primaryColorDark,
                ),
                onPressed: () {
                  setState(() {
                    _passwordVisible = !_passwordVisible;
                  });
                },
              ),
            ),
            validator: (value) => validate(value, [empty, min(6)]),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              onPressed: () => _signUpButton(context),
              child: Text('Registrarse'),
            ),
          ),
        ],
      ),
    );
  }

  void _signUpButton(context) async {
    if (_formKey.currentState.validate()) {
      try {
        setState(() {
          _isLoading = true;
        });
        try {
          await _usuarioProvider.createUsuario(_usr);
          setState(() {
            _isLoading = false;
          });
          Navigator.pushNamed(context, 'login');
        } catch (error) {
          setState(() {
            _isLoading = false;
          });
          if (error is String) {
            showMessage(context, error);
          } else {
            showMessage(context, 'Ocurrio un error al registrarse');
          }
        }
      } catch (error) {
        setState(() {
          _isLoading = false;
        });
        // Scaffold.of(context).showSnackBar(SnackBar(content: Text('Ocurrio un error')));
      }
    }
  }
}
