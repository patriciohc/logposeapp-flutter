import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logposeapp/blocs/account/account_bloc.dart';
import 'package:logposeapp/blocs/account/account_state.dart';
import 'package:logposeapp/blocs/shoping_cart/shoping_cart_bloc.dart';
import 'package:logposeapp/models/cart_model.dart';
import 'package:logposeapp/utils/dialogs.dart';

class CartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          title: Text('Mis productos'),
        ),
        body: _createBody());
  }

  Widget _createBody() {
    return BlocBuilder<ShopingCartBloc, ShopingCartState>(
        builder: (context, state) {
      if (state is ShopingCartUpdate) {
        return _createListView(state.carts, context);
      } else {
        return Center(child: Text('No hay elementos en el carrito de compra'));
      }
    });
  }

  Widget _createListView(List<CartModel> carts, BuildContext context) {
    return ListView(
        shrinkWrap: true,
        children: carts.map((cart) {
          return ExpansionTile(
            initiallyExpanded: true,
            title: Text(
              cart.unidad.nombreEmpresa,
              style: TextStyle(
                color: Colors.teal,
                fontWeight: FontWeight.w500,
              ),
            ),
            children: <Widget>[
              _createListItems(cart, context),
              BlocBuilder<AccountBloc, AccountState>(
                builder: (context, state) {
                  if (state is AccountLoadSuccess) {
                    return _createTotal(cart, context, true);
                  } else {
                    return _createTotal(cart, context, false);
                  }
                },
              ),
              SizedBox(height: 5.0),
            ],
          );
        }).toList());
  }

  Widget _createListItems(CartModel cart, BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: cart.listaProductos.map((element) {
        return Padding(
            padding: EdgeInsets.all(10.0),
            child: _createItem(element, cart, context));
      }).toList(),
    );
  }

  Widget _createItem(
      ProductoVenta element, CartModel cart, BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(element.producto.nombre),
            SizedBox(height: 5.0),
            Text('Cantidad: ' + element.cantidad.toString())
          ],
        ),
        Row(
          children: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.add,
                  color: Colors.teal,
                ),
                onPressed: () {
                  BlocProvider.of<ShopingCartBloc>(context).add(
                      AddElementToCartEvent(element.producto, cart.unidad));
                }),
            IconButton(
                icon: Icon(
                  Icons.remove,
                  color: Colors.red,
                ),
                onPressed: () {
                  BlocProvider.of<ShopingCartBloc>(context).add(
                      RemoveElementToCartEvent(element.producto, cart.unidad));
                }),
          ],
        )
      ],
    );
  }

  Widget _createTotal(CartModel cart, BuildContext context, bool initSession) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Text(
          'Total: \$' + cart.total.toString(),
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
        SizedBox(width: 20.0),
        RaisedButton(
          color: Colors.teal,
          child: Text(
            'Continuar',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            if (initSession) {
              Navigator.pushNamed(context, 'opciones-entrega', arguments: cart);
            } else {
              showMessage(context, 'Debes iniciar sesión para poder continuar');
            }
          },
        ),
        SizedBox(width: 20.0)
      ],
    );
  }
}
