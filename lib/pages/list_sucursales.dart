import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_image/network.dart';
import 'package:logposeapp/blocs/sucursal_list/sucursal_list.dart';
import 'package:logposeapp/blocs/sucursal_list/sucursal_list_bloc.dart';
import 'package:logposeapp/models/sucursal_model.dart';

class ListSucursales extends StatefulWidget {
  @override
  _ListSucursales createState() {
    return _ListSucursales();
  }
}

class _ListSucursales extends State<ListSucursales> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          title: Text('Lista de sucursales'),
        ),
        body: _createBody());
  }

  Widget _createBody() {
    return BlocBuilder<SucursalListBloc, SucursalListState>(
        builder: (context, state) {
      if (state is SucursalFindSuccess) {
        return _createList(state.sucursales, context);
      } else if (state is SucursalListFindIsLoading) {
        return Center(child: CircularProgressIndicator());
      } else {
        return Center(child: Text('Ocurrio un error al buscar sucursales'));
      }
    });
  }

  Widget _createList(List<SucursalModel> sucursales, BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: sucursales.map((item) {
        return Stack(
          children: [
            ListTile(
              title: Text(item.nombreEmpresa),
              subtitle: Text(item.direccion),
              leading: FadeInImage(
                image: NetworkImageWithRetry(item.logo),
                placeholder: AssetImage('assets/img/loading.gif'),
                fit: BoxFit.cover,
              ),
              onTap: () {
                Navigator.pushNamed(context, 'sucursal', arguments: item);
              },
            ),
            if (item.activo)
              Positioned(
                left: 20,
                bottom: 12,
                child: Container(
                  width: 10.0,
                  height: 10.0,
                  decoration: BoxDecoration(
                    color: Color(0xff83F52C),
                    borderRadius: BorderRadius.all(
                      Radius.circular(50.0),
                    ),
                  ),
                  child: Center(
                    child: Text(''),
                  ),
                ),
              )
          ],
        );
      }).toList(),
    );
  }
}
