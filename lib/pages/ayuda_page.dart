import 'package:flutter/material.dart';

class AyudaPage extends StatelessWidget {
  final textStyles = TextStyle(
    fontSize: 18.0,
    color: Colors.black,
  );

  @override
  Widget build(BuildContext superContext) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text('Ayuda'),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: ListView(
          children: <Widget>[
            SizedBox(height: 10.0),
            Text(
              'Gracias por usar LogposeAPP, esta aplicación te permite encontrar servicios o productos que estén cerca de ti, así como también poder programar entregas en tu domicilio o en la sucursal donde realizaste tu pedido.',
              style: textStyles,
            ),
            SizedBox(height: 20.0),
            Text(
              'Si solo quieres buscar algún producto o servicio cerca de ti no necesitas iniciar sesión, da click en el botón saltar y continuar.',
              style: textStyles,
            ),
            SizedBox(height: 5.0),
            Image(image: AssetImage('assets/img/login.png')),
            Text(
              'Puedes indicar la categoría a la que pertenece lo que buscas, también puedes indicar qué tan cerca quieres encontrarlo, da click en el botón buscar para comenzar tu búsqueda.',
              style: textStyles,
            ),
            SizedBox(height: 20.0),
            Image(image: AssetImage('assets/img/home.png')),
            SizedBox(height: 20.0),
            Text(
              'Puedes agregar los negocios más frecuentes que visitas, a la lista de accesos directos, dando clic al botón con la estrella.',
              style: textStyles,
            ),
            SizedBox(height: 20.0),
            Image(image: AssetImage('assets/img/sucursal-1.png')),
            SizedBox(height: 10.0),
            Text(
              'Estos negocios o sucursales aparecerán en la página de inicio para que después puedas acceder fácilmente a ellos sin realizar alguna búsqueda.',
              style: textStyles,
            ),
            SizedBox(height: 20.0),
            Image(image: AssetImage('assets/img/home-1.png')),
            SizedBox(height: 20.0),
            Text(
              'Además si tienes alguna tarjeta de presentación de algún negocio, solo tienes que escanear el código qr para ir directamente a ella.',
              style: textStyles,
            ),
            SizedBox(height: 20.0),
            Image(image: AssetImage('assets/img/home-2.png')),
            SizedBox(height: 30.0),
            RichText(
              text: TextSpan(
                style: new TextStyle(
                  fontSize: 14.0,
                  color: Colors.black,
                ),
                children: <TextSpan>[
                  TextSpan(
                    text:
                        'Si tienes alguna duda o comentario, o tienes alguna negocio y necesitas soluciones tecnológicas, no dudes en comunicarte con nosotros al correo',
                    style: textStyles,
                  ),
                  TextSpan(
                    text: ' soporte@logposeapp.com',
                    style:
                        TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
