import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logposeapp/blocs/account/account_bloc.dart';
import 'package:logposeapp/blocs/categorias/categoria_bloc.dart';
import 'package:logposeapp/blocs/comprador/comprador_bloc.dart';
import 'package:logposeapp/blocs/pedidos/pedidos_bloc.dart';
import 'package:logposeapp/blocs/products/products_bloc.dart';
import 'package:logposeapp/blocs/shoping_cart/shoping_cart_bloc.dart';
import 'package:logposeapp/blocs/sucursal/sucursal_bloc.dart';
import 'package:logposeapp/blocs/sucursal_list/sucursal_list_bloc.dart';
import 'package:logposeapp/pages/ayuda_page.dart';
import 'package:logposeapp/pages/cart_page.dart';
import 'package:logposeapp/pages/home_page.dart';
import 'package:logposeapp/pages/list_pedidos_page.dart';
import 'package:logposeapp/pages/list_sucursales.dart';
import 'package:logposeapp/pages/login_page.dart';
import 'package:logposeapp/pages/opciones_entrega_page.dart';
import 'package:logposeapp/pages/reset_pass_page.dart';
import 'package:logposeapp/pages/signup_page.dart';
import 'package:logposeapp/pages/sucursal_page.dart';
import 'package:logposeapp/pages/ubicacion_page.dart';
import 'package:logposeapp/providers/account_provider.dart';
import 'package:logposeapp/providers/categorias_provider.dart';
import 'package:logposeapp/providers/compra_provider.dart';
import 'package:logposeapp/providers/comprador_provider.dart';
import 'package:logposeapp/providers/products_prodiver.dart';
import 'package:logposeapp/providers/sucursal_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(context) {
    final accountProvider = AccountProvider();
    final categoriasProvider = CategoriasProvider();
    final sucursalProvider = SucursalProvider();
    final productsProvider = ProductsProvider();
    final compraProvider = CompraProvider();
    final compradorProvider = CompradorProvider();

    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => PedidosBloc(compraProvider),
        ),
        BlocProvider(create: (context) => CompradorBloc(compradorProvider)),
        BlocProvider(
          create: (context) => AccountBloc(
            accountProvider,
            BlocProvider.of<CompradorBloc>(context),
          ),
        ),
        BlocProvider(create: (context) => CategoriaBloc(categoriasProvider)),
        BlocProvider(create: (context) => SucursalListBloc(sucursalProvider)),
        BlocProvider(
          create: (context) => ShopingCartBloc(
            compraProvider,
            BlocProvider.of<AccountBloc>(context),
            BlocProvider.of<PedidosBloc>(context),
          ),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'LogposeApp',
        initialRoute: 'login',
        routes: {
          'login': (BuildContext context) => LoginPage(),
          'home': (BuildContext context) => HomePage(),
          'signup': (BuildContext context) => SignUpPage(),
          'reset-pass': (BuildContext context) => ResetPassPage(),
          'list-sucursales': (BuildContext context) => ListSucursales(),
          'sucursal': (BuildContext context) => MultiBlocProvider(
                providers: [
                  BlocProvider(
                    create: (context) => ProductsBloc(productsProvider),
                  ),
                  BlocProvider(
                    create: (context) => SucursalBloc(sucursalProvider),
                  ),
                ],
                child: SucursalPage(),
              ),
          'cart': (BuildContext context) => CartPage(),
          'opciones-entrega': (BuildContext context) => OpcionesEntregaPage(),
          'ubicacion': (BuildContext context) => UbicacionPage(),
          'pedidos': (BuildContext context) => ListaPedidosPage(),
          'ayuda': (BuildContext context) => AyudaPage()
        },
      ),
    );
  }
}
