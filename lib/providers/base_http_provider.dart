import 'dart:convert';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;

import 'package:logposeapp/enviroments.dart';
import 'package:logposeapp/models/base_http_model.dart';

class BaseHttpProvider {
  final FirebaseAuth _fauth = FirebaseAuth.instance;

  FirebaseUser _fAuthSate;

  BaseHttpProvider() {
    _fauth.onAuthStateChanged.listen((event) async {
      if (event != null) {
        _fAuthSate = event;
      }
    });
  }

  final String _urlBase = enviroment['logposeapi'];

  Future<Map<String, dynamic>> post(String operation, BaseHttpModel model,
      {Map<String, String> headers}) async {
    final url = '$_urlBase$operation';
    final body = model.convertModelToJson();
    http.Response response;
    try {
      final headersResult = await _getHeaders(headers);
      response = await http.post(url, headers: headersResult, body: body);
    } catch (error) {
      print(error);
      throw ('Ocurrio un error desconocido');
    }
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw (response.body);
    }
  }

  Future<Map<String, dynamic>> put(String operation,
      {BaseHttpModel model, Map<String, String> headers}) async {
    final url = '$_urlBase$operation';
    http.Response response;
    final headersResult = await _getHeaders(headers);
    if (model != null) {
      final body = model.convertModelToJson();
      try {
        response = await http.put(url, headers: headersResult, body: body);
      } catch (error) {
        print(error);
        throw ('Ocurrio un error desconocido');
      }
    } else {
      try {
        response = await http.put(url, headers: headersResult);
      } catch (error) {
        print(error);
        throw ('Ocurrio un error desconocido');
      }
    }

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw (response.body);
    }
  }

  Future<dynamic> get(String operation, {Map<String, String> headers}) async {
    final url = '$_urlBase$operation';
    http.Response response;
    try {
      final headersResult = await _getHeaders(headers);
      response = await http.get(url, headers: headersResult);
    } on SocketException catch (e) {
      print(e.toString());
      throw ('No hay conexion a internet o el serividor no responde');
    } catch (error) {
      throw ('Ocurrio un error desconocido');
    }
    if (response.statusCode == 200) {
      return json.decode(response.body);
    }
    if (response.statusCode == 401) {
      throw ('Usuario o contraseña no valido');
    } else {
      throw ('Ocurrio un error');
    }
  }

  Future<Map<String, String>> _getHeaders(Map<String, String> headers) async {
    if (headers != null) {
      headers['Content-Type'] = 'application/json';
    } else {
      headers = {"Content-Type": "application/json"};
    }
    if (_fAuthSate != null) {
      try {
        final IdTokenResult idToken = await _fAuthSate.getIdToken();
        if (idToken != null) {
          final String token = idToken.token;
          print(token);
          headers['Authorization'] = 'Bearer $token';
        }
      } catch (error) {
        print('no se ha iniciado sesion');
      }
    }
    return headers;
  }
}
