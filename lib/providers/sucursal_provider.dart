import 'package:logposeapp/models/sucursal_model.dart';
import 'package:logposeapp/providers/base_http_provider.dart';

class SucursalProvider extends BaseHttpProvider {
  Future<List<SucursalModel>> findSucursales(
      {double lat, double lng, double proximidad, String categoriaId}) async {
    String url = '/unidad/public/';
    String query = '';
    if (categoriaId != null) {
      query = 'categoriaId=$categoriaId';
    }
    if (lat != null && lng != null && proximidad != null) {
      query += '&lat=$lat&lng=$lng&proximidad=$proximidad';
    }
    if (query != '') {
      url = url + '?' + query;
    }
    final List<dynamic> data = await this.get(url);
    final List<SucursalModel> result = List();
    data.forEach((element) {
      result.add(SucursalModel.fromJson(element));
    });
    return result;
  }

  Future<SucursalModel> findSucursal(String sucursalId) async {
    String url = '/unidad/public/$sucursalId';
    final data = await this.get(url);
    return SucursalModel.fromJson(data);
  }

  Future<bool> isActive(String sucursalId) async {
    String url = '/unidad/$sucursalId/isActivo';
    final data = await this.get(url);
    return data['data'];
  }
}
