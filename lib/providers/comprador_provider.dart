
import 'package:logposeapp/models/comprador_model.dart';
import 'package:logposeapp/providers/base_http_provider.dart';

class CompradorProvider extends BaseHttpProvider {

  Future<CompradorModel> getDataComprador () async {
    final Map data = await this.get('/comprador');
    return CompradorModel.formJson(data);
  }

  Future<bool> addFavoritos (String sucursalId) async {
    final Map data = await this.put('/comprador/favoritos/$sucursalId');
    return data['success'];
  }
  

}

