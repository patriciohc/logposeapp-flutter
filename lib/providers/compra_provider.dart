import 'package:logposeapp/models/cart_model.dart';
import 'package:logposeapp/providers/base_http_provider.dart';

class CompraProvider extends BaseHttpProvider {
  Future<CartModel> hacerCompra(CartModel cart) async {
    final data = await this.post('/venta/online', cart);
    return CartModel.fromJson(data);
  }

  Future<List<CartModel>> getListaPedidos() async {
    final data = await this.get('/comprador/compras');
    return data.map<CartModel>((item) => CartModel.fromJson(item)).toList();
  }
}
