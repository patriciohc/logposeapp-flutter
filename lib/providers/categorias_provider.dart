
import 'package:logposeapp/models/categoria_model.dart';
import 'package:logposeapp/providers/base_http_provider.dart';

class CategoriasProvider extends BaseHttpProvider {

  Future<List<CategoriaModel>> getCategorias () async {
    final List<dynamic> data = await this.get('/categoria-socio');
    final List<CategoriaModel> result = List();
    data.forEach((element) { 
      result.add(CategoriaModel.fromJson(element));
    });
    return result;
  }

}

