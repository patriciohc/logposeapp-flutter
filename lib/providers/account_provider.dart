import 'package:firebase_auth/firebase_auth.dart';
import 'package:logposeapp/models/account_model.dart';
import 'package:logposeapp/providers/base_http_provider.dart';
import 'dart:convert';

class AccountProvider extends BaseHttpProvider {


  final FirebaseAuth _fauth = FirebaseAuth.instance;

  Future<void> login(String usuario, String password) async {
    final refreshToken = await _getRefreshToken(usuario, password);
    await _signWithToken(refreshToken);
  }

  Future<void> logout() async {
    return _fauth.signOut();
  }

  Future<String> _getRefreshToken ( String usuario, String password ) async {
    final bytes = utf8.encode('$usuario:$password');
    final base64Str = base64.encode(bytes);
    final headers = {
      'Authorization': 'Basic $base64Str'
    };
    final data = await this.get('/account/tokenrefresh?tipo=comprador', headers: headers);
    return data['token'];
  }

  Future<AccountModel> laodAccount() async {
    final data = await this.get('/account');
    return AccountModel.fromJson(data);
  }

  _signWithToken(String refreshToken) async {
    final AuthResult authResult = await _fauth.signInWithCustomToken(token: refreshToken);
    final token = await authResult.user.getIdToken();
    return token;
  }

  get onAuthStateChanged => _fauth.onAuthStateChanged;

  get currentUser => _fauth.currentUser;

}

