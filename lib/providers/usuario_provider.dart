import 'package:logposeapp/models/email_model.dart';
import 'package:logposeapp/models/usuario_model.dart';
import 'package:logposeapp/providers/base_http_provider.dart';

class UsuarioProvider extends BaseHttpProvider {
  Future<UsuarioModel> createUsuario(UsuarioModel producto) async {
    final data = await this.post('/usuario', producto);
    return UsuarioModel.fromJson(data);
  }

  Future<UsuarioModel> resetPassword(EmailModel email) async {
    final data = await this.post('/account/recuperar-contrasena', email);
    return UsuarioModel.fromJson(data);
  }
}
