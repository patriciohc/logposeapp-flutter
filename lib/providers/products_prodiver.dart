
import 'package:logposeapp/models/product_model.dart';
import 'package:logposeapp/providers/base_http_provider.dart';

class ProductsProvider extends BaseHttpProvider {

  Future<List<ProductModel>> getProducts(
    String unidadId, {String categoriaId, String texto}
  ) async {
    String url = '/producto/public/?unidadId=$unidadId';
    if (categoriaId != null) {
      url += '&categoriaId=$categoriaId';
    }
    if (texto != null && texto != '') {
      url += '&texto=$texto';
    }
    final List<dynamic> data = await this.get(url);
    final List<ProductModel> result = List();
    data.forEach((element) {
      result.add(ProductModel.fromJson(element));
    });
    return result;
  }

}

