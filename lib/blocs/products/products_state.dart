part of 'products_bloc.dart';

@immutable
abstract class ProductsState  extends Equatable {
    
  const ProductsState();

  @override
  List<Object> get props => [];
}

class ProductsInitial extends ProductsState {}

class ProductsIsLoading extends ProductsState {}

class ProductsLoadFailed extends ProductsState {}

class ProductsLoadSuccess extends ProductsState {

  final List<ProductModel> products;

  ProductsLoadSuccess(this.products);

  @override
  List<Object> get props => [products];  

}
