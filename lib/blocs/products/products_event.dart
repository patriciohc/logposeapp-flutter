part of 'products_bloc.dart';

@immutable
abstract class ProductsEvent extends Equatable {}

class ProductsLoadEvent extends ProductsEvent {
  
  final String unidadId;
  final String categoriaId;
  final String texto;

  ProductsLoadEvent(this.unidadId, {this.categoriaId, this.texto});

  @override
  List<Object> get props => [unidadId];

}


