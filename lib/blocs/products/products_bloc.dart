import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:logposeapp/models/product_model.dart';
import 'package:logposeapp/providers/products_prodiver.dart';
import 'package:meta/meta.dart';

part 'products_event.dart';
part 'products_state.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final ProductsProvider productsProvider;

  ProductsBloc(this.productsProvider) : super(ProductsInitial());

  @override
  Stream<ProductsState> mapEventToState(ProductsEvent event) async* {
    if (event is ProductsLoadEvent) {
      yield* _loadProducts(event);
    }
  }

  Stream<ProductsState> _loadProducts(ProductsLoadEvent event) async* {
    yield ProductsIsLoading();
    try {
      final products = await productsProvider.getProducts(event.unidadId,
          categoriaId: event.categoriaId, texto: event.texto);
      yield ProductsLoadSuccess(products);
    } catch (error) {
      print(error);
      yield ProductsLoadFailed();
    }
  }
}
