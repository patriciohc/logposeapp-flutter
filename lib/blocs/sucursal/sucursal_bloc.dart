import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:logposeapp/models/sucursal_model.dart';
import 'package:logposeapp/providers/sucursal_provider.dart';

part 'sucursal_event.dart';
part 'sucursal_state.dart';

class SucursalBloc extends Bloc<SucursalEvent, SucursalState> {
  final SucursalProvider sucursalProvier;

  SucursalBloc(this.sucursalProvier) : super(SucursalInitial());

  @override
  Stream<SucursalState> mapEventToState(
    SucursalEvent event,
  ) async* {
    if (event is SucursalFindEvent) {
      yield* _findSucursal(event);
    }
  }

  Stream<SucursalState> _findSucursal(SucursalFindEvent event) async* {
    yield SucursalFindIsLoading();
    try {
      final sucursal =
          await this.sucursalProvier.findSucursal(event.sucursalId);
      yield SucursalFindSuccess(sucursal);
    } catch (error) {
      print(error);
      yield SucursalFindFiled('Error al busar la sucursal');
    }
  }
}
