part of 'sucursal_bloc.dart';

abstract class SucursalEvent extends Equatable {
  const SucursalEvent();
}

class SucursalFindEvent extends SucursalEvent {
  final String sucursalId;

  const SucursalFindEvent(this.sucursalId);

  @override
  List<Object> get props => [this.sucursalId];
}
