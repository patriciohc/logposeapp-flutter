part of 'sucursal_bloc.dart';

abstract class SucursalState extends Equatable {
  const SucursalState();
}

class SucursalInitial extends SucursalState {
  @override
  List<Object> get props => [];
}

class SucursalFindIsLoading extends SucursalState {
  @override
  List<Object> get props => [];
}

class SucursalFindFiled extends SucursalState {
  final String message;

  SucursalFindFiled(this.message);

  @override
  List<Object> get props => [message];
}

class SucursalFindSuccess extends SucursalState {
  final SucursalModel sucursal;

  SucursalFindSuccess(this.sucursal);

  @override
  List<Object> get props => [sucursal];
}
