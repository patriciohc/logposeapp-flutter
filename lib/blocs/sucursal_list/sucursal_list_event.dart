import 'package:equatable/equatable.dart';

abstract class SucursalListEvent extends Equatable {
  const SucursalListEvent();
}

class SucursalListFindEvent extends SucursalListEvent {
  final double proximidad;
  final String categoriaId;

  @override
  List<Object> get props => [];

  SucursalListFindEvent(this.proximidad, this.categoriaId);
}
