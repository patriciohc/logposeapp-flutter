import 'package:equatable/equatable.dart';
import 'package:logposeapp/models/sucursal_model.dart';

abstract class SucursalListState extends Equatable {
  const SucursalListState();

  @override
  List<Object> get props => [];
}

class SucursalListInitialState extends SucursalListState {}

class SucursalListFind extends SucursalListState {}

class SucursalListFindIsLoading extends SucursalListState {}

class SucursalListFindFiled extends SucursalListState {
  final String message;

  SucursalListFindFiled(this.message);

  @override
  List<Object> get props => [message];
}

class SucursalFiled extends SucursalListState {}

class SucursalFindSuccess extends SucursalListState {
  final List<SucursalModel> sucursales;

  SucursalFindSuccess(this.sucursales);

  @override
  List<Object> get props => [sucursales];
}
