import 'package:geolocator/geolocator.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logposeapp/blocs/sucursal_list/sucursal_list.dart';
import 'package:logposeapp/providers/sucursal_provider.dart';

class SucursalListBloc extends Bloc<SucursalListEvent, SucursalListState> {
  final SucursalProvider sucursalProvier;

  SucursalListBloc(this.sucursalProvier) : super(SucursalListInitialState());

  @override
  Stream<SucursalListState> mapEventToState(SucursalListEvent event) async* {
    if (event is SucursalListFindEvent) {
      yield* _findSucursales(event);
    }
  }

  Stream<SucursalListState> _findSucursales(
      SucursalListFindEvent event) async* {
    yield SucursalListFindIsLoading();
    if (event.proximidad == 0.3) {
      final sucursales =
          await sucursalProvier.findSucursales(categoriaId: event.categoriaId);
      yield SucursalFindSuccess(sucursales);
    } else {
      Position pos;
      try {
        pos = await Geolocator()
            .getCurrentPosition(desiredAccuracy: LocationAccuracy.medium)
            .timeout(Duration(seconds: 5));
      } catch (error) {
        print(error);
        SucursalListFindFiled('No se puedo obtener su posicion actual');
      }
      try {
        final sucursales = await sucursalProvier.findSucursales(
            lat: pos.latitude,
            lng: pos.longitude,
            proximidad: event.proximidad,
            categoriaId: event.categoriaId);
        yield SucursalFindSuccess(sucursales);
      } catch (error) {
        print(error);
        SucursalListFindFiled(
          'Ocurrio un error al consultar la lista de sucursales',
        );
      }
    }
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
