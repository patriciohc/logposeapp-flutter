part of 'comprador_bloc.dart';

abstract class CompradorEvent extends Equatable {
  const CompradorEvent();
}


class CompradorLoadEvent extends CompradorEvent {
  @override
  List<Object> get props => [];
}

class CompradorAddFavoritosEvent extends CompradorEvent {

  final SucursalModel sucursal;

  const CompradorAddFavoritosEvent(this.sucursal);

  @override
  List<Object> get props => [];

}