part of 'comprador_bloc.dart';

abstract class CompradorState extends Equatable {
  const CompradorState();
}

class CompradorInitial extends CompradorState {
  @override
  List<Object> get props => [];
}

class CompradorIsLoading extends CompradorState {
  @override
  List<Object> get props => [];
}

class CompradorLoadSuccess extends CompradorState {

  final CompradorModel comprador;
  final timestamp;

  const CompradorLoadSuccess(this.comprador, this.timestamp);

  @override
  List<Object> get props => [comprador, timestamp];
}

class CompradorLoadFiled extends CompradorState {

  final String message;

  @override
  List<Object> get props => [message];

  const CompradorLoadFiled(this.message);
}