import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:logposeapp/models/comprador_model.dart';
import 'package:logposeapp/models/sucursal_model.dart';
import 'package:logposeapp/providers/comprador_provider.dart';

part 'comprador_event.dart';
part 'comprador_state.dart';

class CompradorBloc extends Bloc<CompradorEvent, CompradorState> {
  final CompradorProvider compradorProvider;
  CompradorModel comprador;

  CompradorBloc(this.compradorProvider) : super(CompradorInitial()) {
    this.comprador = CompradorModel();
  }

  @override
  Stream<CompradorState> mapEventToState(
    CompradorEvent event,
  ) async* {
    if (event is CompradorLoadEvent) {
      yield* _loadDataComprador();
    } else if (event is CompradorAddFavoritosEvent) {
      yield* _addFavoritos(event);
    }
  }

  Stream<CompradorState> _loadDataComprador() async* {
    yield CompradorIsLoading();
    try {
      comprador = await compradorProvider.getDataComprador();
      yield CompradorLoadSuccess(comprador, null);
    } catch (error) {
      yield CompradorLoadFiled('Erro al cargar datos del perfil');
    }
  }

  Stream<CompradorState> _addFavoritos(
      CompradorAddFavoritosEvent event) async* {
    try {
      bool success = await compradorProvider.addFavoritos(event.sucursal.id);
      if (success) {
        this.comprador.favoritos.add(event.sucursal);
        yield CompradorLoadSuccess(comprador, DateTime.now());
      }
    } catch (error) {
      print(error);
    }
  }
}
