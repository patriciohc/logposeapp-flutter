import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:logposeapp/blocs/account/account_event.dart';
import 'package:logposeapp/blocs/account/account_state.dart';
import 'package:logposeapp/blocs/comprador/comprador_bloc.dart';
import 'package:logposeapp/providers/account_provider.dart';

class AccountBloc extends Bloc<AccountEvent, AccountState> {
  final AccountProvider accountProvider;
  final CompradorBloc compradorBloc;

  AccountBloc(
    this.accountProvider,
    this.compradorBloc,
  ) : super(AccountNotSession()) {
    accountProvider.onAuthStateChanged.listen((event) {
      if (event != null) {
        add(AccountLoadEvent());
        compradorBloc.add(CompradorLoadEvent());
      }
    });
  }

  @override
  Stream<AccountState> mapEventToState(AccountEvent event) async* {
    if (event is AccountLoadEvent) {
      yield* _loadAccount();
    } else if (event is LogInEvent) {
      yield* _login(event);
    } else if (event is HideErrorEvent) {
      yield HideError();
    } else if (event is AccountLogoutEvent) {
      yield* _logout();
    }
  }

  Stream<AccountState> _loadAccount() async* {
    yield AccountLoadInProgress();
    try {
      final account = await accountProvider.laodAccount();
      yield AccountLoadSuccess(account);
    } catch (error) {
      yield AccountLoadError(error.toString());
    }
  }

  Stream<AccountState> _login(LogInEvent e) async* {
    yield LoggingIn();
    try {
      await accountProvider.login(e.usuario, e.password);
      yield LoginSuccess();
    } catch (error) {
      print(error);
      yield LoginFailed(error.toString());
    }
  }

  Stream<AccountState> _logout() async* {
    await accountProvider.logout();
    yield AccountNotSession();
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
