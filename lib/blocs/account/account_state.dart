import 'package:equatable/equatable.dart';
import 'package:logposeapp/models/account_model.dart';

abstract class AccountState extends Equatable {

  const AccountState();

  @override
  List<Object> get props => [];
}


class AccountNotSession extends AccountState {}

class AccountLoadInProgress extends AccountState {}

class AccountLoadSuccess extends AccountState {

  final AccountModel account;

  const AccountLoadSuccess(this.account);

  @override
  List<Object> get props => [account];

  @override
  String toString() {
    return 'AccountLoadSuccess { account: $account }';
  }

}

class AccountLoadError extends AccountState {

  final String message;
  const AccountLoadError(this.message);

  @override
  List<Object> get props => [message];

  @override
  String toString() => 'AccountLoadError { message: $message }';

}

class LoggingIn extends AccountState {}

class LoginSuccess extends AccountState {}

class LoginFailed extends AccountState {

  final String message;
  const LoginFailed(this.message);

  @override
  List<Object> get props => [message];

  @override
  String toString() => 'LoginFailed { message: $message }';

}

class HideError extends AccountState {}