import 'package:equatable/equatable.dart';
import 'package:logposeapp/models/account_model.dart';

abstract class AccountEvent extends Equatable {
  const AccountEvent();
}

class LogInEvent extends AccountEvent {

  final String usuario;
  final String password;

  const LogInEvent(this.usuario, this.password);

  @override
  List<Object> get props => [usuario, password];

  @override
  String toString() => 'LogInEvent { user: $usuario }';
}

class AccountLoadEvent extends AccountEvent {

  const AccountLoadEvent();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'AccountLoadEvent';
}

class AccountLoadSuccessEvent extends AccountEvent {

  final AccountModel account;

  const AccountLoadSuccessEvent(this.account);

  @override
  List<Object> get props => [account];

  @override
  String toString() => 'AccountLoadEvent { account: $account }';
}

class HideErrorEvent extends AccountEvent {
  @override
  List<Object> get props => [];
}

class AccountLogoutEvent extends AccountEvent {
  @override
  List<Object> get props => [];
}