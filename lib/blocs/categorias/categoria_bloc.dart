import 'dart:async';
import 'package:bloc/bloc.dart';

import 'package:logposeapp/providers/categorias_provider.dart';
import 'package:logposeapp/blocs/categorias/categoria_event.dart';
import 'package:logposeapp/blocs/categorias/categoria_state.dart';

class CategoriaBloc extends Bloc<CategoriaEvent, CategoriaState> {
  final CategoriasProvider categoriasProvider;

  CategoriaBloc(this.categoriasProvider) : super(CategoriaInitial()) {
    this.add(CategoriasLoadEvent());
  }

  @override
  Stream<CategoriaState> mapEventToState(CategoriaEvent event) async* {
    if (event is CategoriasLoadEvent) {
      yield* _loadCategorias();
    }
  }

  Stream<CategoriaState> _loadCategorias() async* {
    yield CategoriaIsLoading();
    try {
      final categorias = await categoriasProvider.getCategorias();
      yield CategoriaLoadSuccess(categorias);
    } catch (error) {
      print(error);
      yield CategoriaLoadFailed('No se puedo cargar la lista de categorias');
    }
  }
}
