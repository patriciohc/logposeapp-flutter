import 'package:equatable/equatable.dart';
import 'package:logposeapp/models/categoria_model.dart';

abstract class CategoriaState extends Equatable {
  const CategoriaState();
}

class CategoriaInitial extends CategoriaState {
  @override
  List<Object> get props => [];
}

class CategoriaIsLoading extends CategoriaState {
    @override
    List<Object> get props => [];
}

class CategoriaLoadFailed extends CategoriaState {

  final String message;

  CategoriaLoadFailed(this.message);  
    
  @override
  List<Object> get props => [message];
}

class CategoriaLoadSuccess extends CategoriaState {

  final List<CategoriaModel> categorias;

  const CategoriaLoadSuccess(this.categorias);

  @override
  List<Object> get props => [categorias];
}
