
import 'package:equatable/equatable.dart';

abstract class CategoriaEvent extends Equatable {
  const CategoriaEvent();
}

class CategoriasLoadEvent extends CategoriaEvent {
    @override
    List<Object> get props => [];
}
