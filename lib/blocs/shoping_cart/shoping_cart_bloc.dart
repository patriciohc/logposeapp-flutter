import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:logposeapp/blocs/account/account.dart';
import 'package:logposeapp/blocs/account/account_bloc.dart';
import 'package:logposeapp/blocs/pedidos/pedidos_bloc.dart';
import 'package:logposeapp/models/cart_model.dart';
import 'package:logposeapp/models/product_model.dart';
import 'package:logposeapp/models/sucursal_model.dart';
import 'package:logposeapp/providers/compra_provider.dart';

part 'shoping_cart_event.dart';
part 'shoping_cart_state.dart';

class ShopingCartBloc extends Bloc<ShopingCartEvent, ShopingCartState> {
  final CompraProvider compraProvider;
  final AccountBloc accountBloc;
  final PedidosBloc pedidosBloc;

  List<CartModel> carts;
  String compradorId;

  ShopingCartBloc(this.compraProvider, this.accountBloc, this.pedidosBloc)
      : super(ShopingCartInitial()) {
    carts = new List();
    accountBloc.listen((state) {
      if (state is AccountLoadSuccess) {
        compradorId = state.account.compradorId;
      }
    });
  }

  @override
  Stream<ShopingCartState> mapEventToState(ShopingCartEvent event) async* {
    if (event is AddElementToCartEvent) {
      yield* _addElementToCart(event);
    } else if (event is RemoveElementToCartEvent) {
      yield* _removeElementToCart(event);
    } else if (event is AddInfoEntrega) {
      yield* _addInfoEntrega(event);
    } else if (event is AddDireccionEntrega) {
      yield* _addDireccionEntrega(event);
    } else if (event is ConfirmarPedido) {
      yield* _confirmarPedido(event);
    }
  }

  Stream<ShopingCartState> _confirmarPedido(ConfirmarPedido event) async* {
    final cartFound = carts.firstWhere(
      (element) => element.unidad.id == event.sucursal.id,
      orElse: () => null,
    );

    if (cartFound != null) {
      try {
        yield ShopingCartIsLoading();
        final compra = await compraProvider.hacerCompra(cartFound);
        carts.remove(cartFound);
        yield SendPedidoSuccess();
        await Future.delayed(Duration(seconds: 1));
        yield ShopingCartUpdate(carts, DateTime.now());
        pedidosBloc.add(PedidosAddItemEvent(compra));
      } catch (error) {
        yield SendPedidoFailed(
            'No se pudo enviar el pedido intentalo mas tarder');
        yield ShopingCartInitial();
        print(error);
      }
    }
  }

  Stream<ShopingCartState> _addInfoEntrega(AddInfoEntrega event) async* {
    final cartFound = carts.firstWhere(
      (element) => element.unidad.id == event.sucursal.id,
      orElse: () => null,
    );

    if (cartFound != null) {
      cartFound.addInfoEntrega(event.infoEntrega);
      yield ShopingCartUpdate(carts, DateTime.now());
    }
  }

  Stream<ShopingCartState> _addDireccionEntrega(
    AddDireccionEntrega event,
  ) async* {
    final cartFound = carts.firstWhere(
      (element) => element.unidad.id == event.sucursal.id,
      orElse: () => null,
    );

    if (cartFound != null) {
      cartFound.addDirreccionEntrega(event.lat, event.lng);
    }
  }

  Stream<ShopingCartState> _addElementToCart(
      AddElementToCartEvent event) async* {
    //if (compradorId == null) {
    //  yield ShopingCartNotSession();
    //}

    final cartFound = carts.firstWhere(
        (element) => element.unidad.id == event.sucursal.id,
        orElse: () => null);

    if (cartFound == null) {
      final cart = new CartModel(event.sucursal, compradorId);
      cart.addProducto(event.producto);
      carts.add(cart);
    } else {
      cartFound.addProducto(event.producto);
    }
    yield ShopingCartUpdate(carts, DateTime.now());
  }

  Stream<ShopingCartState> _removeElementToCart(event) async* {
    final cartFound = carts.firstWhere(
        (element) => element.unidad.id == event.sucursal.id,
        orElse: null);
    if (cartFound == null) {
      yield ShopingCartNotCartFound();
    } else {
      cartFound.removeProducto(event.producto);
      if (cartFound.isEmpty) {
        carts.remove(cartFound);
      }
      if (carts.length == 0) {
        yield ShopingCartIsEmpety();
      } else {
        yield ShopingCartUpdate(carts, DateTime.now());
      }
    }
  }
}
