part of 'shoping_cart_bloc.dart';

abstract class ShopingCartState extends Equatable {
  const ShopingCartState();
}

class ShopingCartInitial extends ShopingCartState {
  @override
  List<Object> get props => [];
}

class ShopingCartNotSession extends ShopingCartState {
  @override
  List<Object> get props => [];
}

class ShopingCartUpdate extends ShopingCartState {
  final List<CartModel> carts;
  final timestamp;

  const ShopingCartUpdate(this.carts, this.timestamp);

  @override
  List<Object> get props => [carts, timestamp];
}

class ShopingCartIsEmpety extends ShopingCartState {
  @override
  List<Object> get props => [];
}

class ShopingCartNotCartFound extends ShopingCartState {
  @override
  List<Object> get props => [];
}

class ShopingCartIsLoading extends ShopingCartState {
  @override
  List<Object> get props => [];
}

class SendPedidoSuccess extends ShopingCartState {
  @override
  List<Object> get props => [];
}

class SendPedidoFailed extends ShopingCartState {
  final String msg;

  SendPedidoFailed(this.msg);

  @override
  List<Object> get props => [];
}
