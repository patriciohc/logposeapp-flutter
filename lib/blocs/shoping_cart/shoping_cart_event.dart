part of 'shoping_cart_bloc.dart';

abstract class ShopingCartEvent extends Equatable {
  const ShopingCartEvent();
}

class AddElementToCartEvent extends ShopingCartEvent {
  final ProductModel producto;
  final SucursalModel sucursal;

  AddElementToCartEvent(this.producto, this.sucursal);

  @override
  List<Object> get props => [producto, sucursal];
}

class RemoveElementToCartEvent extends ShopingCartEvent {
  final ProductModel producto;
  final SucursalModel sucursal;

  RemoveElementToCartEvent(this.producto, this.sucursal);

  @override
  List<Object> get props => [producto, sucursal];
}

class AddInfoEntrega extends ShopingCartEvent {
  final InformacionEntrega infoEntrega;
  final SucursalModel sucursal;

  AddInfoEntrega(this.infoEntrega, this.sucursal);

  @override
  List<Object> get props => [infoEntrega];
}

class AddDireccionEntrega extends ShopingCartEvent {
  final double lat;
  final double lng;
  final SucursalModel sucursal;

  const AddDireccionEntrega(this.lat, this.lng, this.sucursal);

  @override
  List<Object> get props => [lat, lng];
}

class ConfirmarPedido extends ShopingCartEvent {
  final SucursalModel sucursal;

  const ConfirmarPedido(this.sucursal);

  @override
  List<Object> get props => [sucursal];
}
