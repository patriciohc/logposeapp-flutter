import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:logposeapp/models/cart_model.dart';
import 'package:logposeapp/providers/compra_provider.dart';

part 'pedidos_event.dart';
part 'pedidos_state.dart';

class PedidosBloc extends Bloc<PedidosEvent, PedidosState> {
  final CompraProvider compraProvider;

  List<CartModel> pedidos = [];

  PedidosBloc(this.compraProvider) : super(PedidosInitial()) {
    add(PedidosGetEvent());
  }

  @override
  Stream<PedidosState> mapEventToState(
    PedidosEvent event,
  ) async* {
    if (event is PedidosGetEvent) {
      yield* _getListPedidos();
    } else if (event is PedidosAddItemEvent) {
      yield* _addItemToListPedidos(event);
    }
  }

  Stream<PedidosState> _addItemToListPedidos(PedidosAddItemEvent event) async* {
    pedidos.add(event.pedido);
    yield PedidosUpdate(pedidos, DateTime.now());
  }

  Stream<PedidosState> _getListPedidos() async* {
    yield PedidosIsLoading();
    try {
      pedidos = await compraProvider.getListaPedidos();
      yield PedidosUpdate(pedidos, DateTime.now());
    } catch (error) {
      print(error);
      yield PedidosLoadFailed('No se pudo cargar la lista de pedidos');
    }
  }
}
