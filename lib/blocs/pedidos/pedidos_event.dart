part of 'pedidos_bloc.dart';

abstract class PedidosEvent extends Equatable {
  const PedidosEvent();
}

class PedidosGetEvent extends PedidosEvent {
  const PedidosGetEvent();

  @override
  List<Object> get props => [];
}

class PedidosAddItemEvent extends PedidosEvent {
  final CartModel pedido;

  const PedidosAddItemEvent(this.pedido);

  @override
  List<Object> get props => [pedido];
}
