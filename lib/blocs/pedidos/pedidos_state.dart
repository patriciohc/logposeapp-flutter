part of 'pedidos_bloc.dart';

abstract class PedidosState extends Equatable {
  const PedidosState();
}

class PedidosInitial extends PedidosState {
  @override
  List<Object> get props => [];
}

class PedidosIsLoading extends PedidosState {
  @override
  List<Object> get props => [];
}

class PedidosLoadFailed extends PedidosState {
  final String message;

  const PedidosLoadFailed(this.message);

  @override
  List<Object> get props => [message];
}

class PedidosUpdate extends PedidosState {
  final timestamp;
  final List<CartModel> compras;

  const PedidosUpdate(this.compras, this.timestamp);

  @override
  List<Object> get props => [compras];
}
