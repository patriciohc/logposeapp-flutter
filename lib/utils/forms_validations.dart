
empty(value) {
  if (value.isEmpty) return 'Este campo es requerido';
}

Function min(min) {
  return (value) {
    return value.length < min ? 'Se requieren almenos $min caracteres': null;
  };
}

Function max(max) {
  return (value) {
    return value.length > max ? 'El liminte es de $max caracteres': null;
  };
}

email(value) {
  final regExp = r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
  if(!RegExp(regExp).hasMatch(value)) {
    return 'Correo electrónico no válido';
  }
}

validate (value, List<Function> funs) {
  for (var i = 0; i < funs.length; i++) {
    final result = funs[i](value);
    if (result != null) return result;
  }
}