import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logposeapp/blocs/shoping_cart/shoping_cart_bloc.dart';

class CartWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        IconButton(
          icon: Icon(Icons.shopping_cart),
          onPressed: () {
            Navigator.pushNamed(context, 'cart');
          },
        ),
        BlocBuilder<ShopingCartBloc, ShopingCartState>(
          builder: (context, state) {
            if (state is ShopingCartUpdate) {
              return Positioned(
                left: 22,
                top: 4,
                child: _createNumber(state),
              );
            } else {
              return Center();
            }
          },
        )
      ],
    );
  }

  _createNumber(ShopingCartUpdate state) {
    int _total = 0;
    state.carts.forEach((element) {
      _total += element.totalProductos;
    });

    return Container(
      width: 20.0,
      height: 20.0,
      decoration: BoxDecoration(
        color: Color(0xff455A64),
        borderRadius: BorderRadius.all(
          Radius.circular(50.0),
        ),
      ),
      child: Center(
        child: Text(
          '$_total',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
          ),
        ),
      ),
    );
  }
}
