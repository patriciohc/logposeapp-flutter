import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logposeapp/blocs/account/account.dart';
import 'package:logposeapp/blocs/account/account_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

class MenuWidget extends StatelessWidget {
  // final _firebaseProvider = FirebaseProvider();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AccountBloc, AccountState>(
      builder: (context, state) {
        return Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: _createMenuItems(context, state),
          ),
        );
      },
    );
  }

  List<Widget> _createMenuItems(context, state) {
    List<Widget> items = [];
    items.add(DrawerHeader(
      child: Container(),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/img/menu-img.jpg'),
          fit: BoxFit.cover,
        ),
      ),
    ));

    items.add(ListTile(
      leading: Icon(Icons.home, color: Colors.teal),
      title: Text('Home'),
      onTap: () => Navigator.pushReplacementNamed(context, 'home'),
    ));

    if (state is AccountLoadSuccess) {
      items.add(ListTile(
        leading: Icon(Icons.list, color: Colors.teal),
        title: Text('Lista de pedidos'),
        onTap: () => Navigator.pushNamed(context, 'pedidos'),
      ));
      items.add(ListTile(
          leading: Icon(Icons.settings, color: Colors.teal),
          title: Text('Configuraciones'),
          onTap: () {
            Navigator.pushNamed(context, 'settings');
          }));
      items.add(ListTile(
          leading: Icon(Icons.exit_to_app, color: Colors.teal),
          title: Text('Salir'),
          onTap: () {
            BlocProvider.of<AccountBloc>(context).add(AccountLogoutEvent());
          }));
    } else {
      items.add(ListTile(
        leading: Icon(Icons.account_circle, color: Colors.teal),
        title: Text('Iniciar sesion'),
        onTap: () {
          Navigator.pushReplacementNamed(context, 'login');
        },
      ));
    }

    items.add(
      ListTile(
        leading: Icon(Icons.help, color: Colors.teal),
        title: Text('Ayuda'),
        onTap: () => Navigator.pushNamed(context, 'ayuda'),
      ),
    );

    items.add(
      ListTile(
        leading: Icon(Icons.announcement, color: Colors.teal),
        title: Text('Aviso de privacidad'),
        onTap: _launchPrivacidad,
      ),
    );

    items.add(
      ListTile(
        leading: Icon(Icons.announcement, color: Colors.teal),
        title: Text('Terminos y condiciones'),
        onTap: _launchTerminos,
      ),
    );

    return items;
  }

  _launchTerminos() async {
    const url = 'https://logposeapp.com/terminos-y-condiciones.html';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchPrivacidad() async {
    const url = 'https://logposeapp.com/aviso-de-privacidad.html';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
