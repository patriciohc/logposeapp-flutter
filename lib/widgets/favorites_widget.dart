import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:logposeapp/models/sucursal_model.dart';

class FavoritesWidget extends StatelessWidget {
  final List<SucursalModel> favorites;

  FavoritesWidget({@required this.favorites});

  final _pageController = PageController(initialPage: 1, viewportFraction: 0.4);

  @override
  Widget build(BuildContext context) {
    // final _screenSize = MediaQuery.of(context).size;

    return Container(
      height: 120.0,
      child: PageView.builder(
        pageSnapping: false,
        controller: _pageController,
        itemCount: favorites.length,
        itemBuilder: (context, i) => _tarjeta(context, favorites[i]),
      ),
    );
  }

  Widget _tarjeta(BuildContext context, SucursalModel surcursal) {
    final tarjeta = Container(
      margin: EdgeInsets.only(right: 15.0),
      child: Column(
        children: <Widget>[
          Hero(
            tag: surcursal.id,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(6.0),
              child: FadeInImage(
                width: 170,
                height: 102,
                placeholder: AssetImage('assets/img/loading.gif'),
                image: NetworkImageWithRetry(surcursal.promocionalImg),
              ),
            ),
          ),
          // SizedBox(height: 5.0),
          Text(
            surcursal.nombreEmpresa,
            style: TextStyle(color: Colors.white),
            // overflow: TextOverflow.ellipsis,
          )
        ],
      ),
    );

    return GestureDetector(
      child: tarjeta,
      onTap: () {
        Navigator.pushNamed(context, 'sucursal', arguments: surcursal);
      },
    );
  }
}
