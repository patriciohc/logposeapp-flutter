import 'package:flutter/material.dart';
import 'package:logposeapp/models/base_http_model.dart';
import 'package:logposeapp/models/product_model.dart';
import 'package:logposeapp/models/sucursal_model.dart';

/*
* Almacena ventas o servicios realizados
*/

enum TipoVenta {
  REGULAR, // lista de productos regulares
  ESPECIAL, // productos especial
}

enum TipoEntrega {
  MOSTRADOR, // venta en mostrador
  DOMICILIO, // entrega a domicilio
}

enum MetodoPago { EFECTIVO, PAYPAL, VISA }

enum EstatusVenta {
  ESPERANDO, // esperando a ser aceptado por la sucursal
  ACEPTADO, // aceptado por la sucursal
  EN_PROGRESO, // en progreso (en camino)
  FINALIZADO, // finalizdo
  CANCELADO // cancelado
}

enum TipoHoraEntrega {
  LO_ANTES_POSIBLE, // no requires hora_entrega
  HORA_PROGRAMADA, // se desea en la hora programada
  DESPUES_HORA_PROGRAMADA // se desea a cualquier hora despues de la hora programada
}

class CartModel extends BaseHttpModel<CartModel> {
  final MetodoPago metodoPago = MetodoPago.EFECTIVO;

  /// lista de productos regulares o
  /// un servicio o producto especial
  final TipoVenta tipo = TipoVenta.REGULAR;

  SucursalModel unidad;
  String compradorId;
  DateTime fecha;
  double subtotal;
  double iva;
  double total;
  List<ProductoVenta> listaProductos = new List();
  InformacionEntrega informacionEntrega;
  EstatusVenta estatus;
  int totalProductos;
  String noTicket;

  CartModel(this.unidad, this.compradorId);

  CartModel.fromJson(Map<String, dynamic> data) {
    noTicket = data['no_ticket'] != null ? data['no_ticket'] : '';
    unidad = SucursalModel.fromJson(data['unidad']);
    if (data['estatus'] != null) {
      estatus = EstatusVenta.values[data['estatus']];
    }

    if (data['fecha'] != null) {
      fecha = DateTime.parse(data['fecha'] + 'Z');
    } else {
      fecha = DateTime.now();
    }
    subtotal = data['subtotal'] != null ? data['subtotal'].toDouble() : 0.0;
    iva = data['iva'] != null ? data['iva'].toDouble() : 0.0;
    total = data['total'] != null ? data['total'].toDouble() : 0.0;
    listaProductos = data['lista_productos']
        .map<ProductoVenta>((item) => ProductoVenta.fromJson(item))
        .toList();
    if (data['informacion_entrega'] != null) {
      informacionEntrega =
          InformacionEntrega.fromJson(data['informacion_entrega']);
    }
  }

  get isEmpty => listaProductos.length == 0;

  addProducto(ProductModel producto) {
    final existent = listaProductos.firstWhere(
      (item) => item.producto.id == producto.id,
      orElse: () => null,
    );

    if (existent != null) {
      existent.cantidad += 1;
      existent.total = existent.producto.precio * existent.cantidad;
      existent.iva = existent.total * producto.iva;
      existent.subtotal = existent.total - existent.iva;
    } else {
      total = producto.precio;
      iva = total * producto.iva;
      subtotal = total - iva;
      listaProductos.add(ProductoVenta(producto,
          cantidad: 1,
          subtotal: subtotal,
          total: total,
          iva: iva,
          descuento: 0));
    }

    calculates();
  }

  calculates() {
    subtotal = 0;
    total = 0;
    iva = 0;
    totalProductos = 0;
    listaProductos.forEach((element) {
      subtotal += element.subtotal;
      total += element.total;
      iva += element.iva;
      totalProductos += element.cantidad;
    });
  }

  removeProducto(ProductModel producto) {
    ProductoVenta existent;

    if (listaProductos.length > 0) {
      existent = listaProductos
          .firstWhere((item) => item.producto.id == producto.id, orElse: null);
    }

    if (existent != null) {
      if (existent.cantidad == 1) {
        listaProductos.remove(existent);
      } else {
        existent.cantidad -= 1;
        existent.total = existent.producto.precio * existent.cantidad;
        existent.iva = existent.total * producto.iva;
        existent.subtotal = existent.total - existent.iva;
      }
      calculates();
    }
  }

  addInfoEntrega(InformacionEntrega infoEntrega) {
    if (informacionEntrega == null) {
      informacionEntrega = new InformacionEntrega();
    }
    informacionEntrega.diaEntrega = infoEntrega.diaEntrega;
    informacionEntrega.horaEntrega = infoEntrega.horaEntrega;
    informacionEntrega.tipoHoraEntrega = infoEntrega.tipoHoraEntrega;
    informacionEntrega.tipoEntrega = infoEntrega.tipoEntrega;
  }

  addDirreccionEntrega(double lat, double lng) {
    if (informacionEntrega == null) {
      informacionEntrega = new InformacionEntrega();
    }
    informacionEntrega.lat = lat;
    informacionEntrega.lng = lng;
  }

  @override
  CartModel createFromJson(Map<String, dynamic> data) {
    return null;
  }

  @override
  Map<String, dynamic> toJson() {
    final json = Map<String, dynamic>();
    json['unidad'] = {'id': unidad.id};
    json['lista_productos'] =
        listaProductos.map((item) => item.toJson()).toList();
    json['tipo'] = tipo.index;
    json['metodo_pago'] = metodoPago.index;
    json['informacion_entrega'] = informacionEntrega.toJson();
    return json;
  }
}

class ProductoVenta {
  ProductModel producto;
  int cantidad;
  double descuento;
  double total;
  double subtotal;
  double iva;

  ProductoVenta(this.producto,
      {this.cantidad, this.descuento, this.total, this.subtotal, this.iva});

  ProductoVenta.fromJson(Map<String, dynamic> data) {
    producto = ProductModel.fromJson(data['producto']);
    cantidad = data['cantidad'];
    total = data['total'] != null ? data['total'].toDouble() : 0.0;
    subtotal = data['subtotal'] != null ? data['subtotal'].toDouble() : 0.0;
    iva = data['iva'] != null ? data['iva'].toDouble() : 0.0;
  }

  toJson() {
    final json = Map<String, dynamic>();
    json['producto'] = producto.toJson();
    json['cantidad'] = cantidad;
    return json;
  }
}

class InformacionEntrega {
  double lat;

  double lng;

  DateTime diaEntrega;

  TimeOfDay horaEntrega;

  TipoHoraEntrega tipoHoraEntrega;

  /// Tipo entrega
  /// Entrega en domicilio
  /// Entrega en mostrador
  TipoEntrega tipoEntrega;

  String horaEntregaConcat;

  toJson() {
    final json = Map<String, dynamic>();

    if (lat != null && lng != null) {
      json['lat'] = lat;
      json['lng'] = lng;
    }

    if (tipoHoraEntrega == TipoHoraEntrega.HORA_PROGRAMADA) {
      json['hora_entrega'] = '$year-$month-$day' + 'T' + '$hour:$minute:00';
    }
    json['tipo_entrega'] = tipoEntrega.index;
    json['tipo_hora_entrega'] = tipoHoraEntrega.index;
    return json;
  }

  InformacionEntrega();

  InformacionEntrega.fromJson(Map<String, dynamic> data) {
    if (data['lat'] != null && data['lng'] != null) {
      lat = data['lat'];
      lng = data['lng'];
    }
    horaEntregaConcat = data['hora_entrega'];
    if (data['tipo_hora_entrega'] != null) {
      tipoHoraEntrega = TipoHoraEntrega.values[data['tipo_hora_entrega']];
    }
    if (data['tipo_entrega'] != null) {
      tipoEntrega = TipoEntrega.values[data['tipo_entrega']];
    }
  }

  get year => diaEntrega != null ? diaEntrega.year : '';
  get month => diaEntrega != null ? diaEntrega.month : '';
  get day => diaEntrega != null ? diaEntrega.day : '';
  get hour => horaEntrega != null ? horaEntrega.hour : '';
  get minute => horaEntrega != null ? horaEntrega.minute : '';
}
