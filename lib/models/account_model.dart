
import 'package:logposeapp/models/base_http_model.dart';
import 'package:logposeapp/models/usuario_model.dart';

class AccountModel extends BaseHttpModel<AccountModel> {

  bool accountIsCreated;
  String perfil;
  String compradorId;
  UsuarioModel usuario;

  AccountModel.fromJson(Map<String, dynamic> data) {
    accountIsCreated = data['accountIsCreated'];
    perfil = data['perfil'];
    compradorId = data['comprador_id'];
    usuario = UsuarioModel.fromJson(data['usuario']);
  }

  @override
  Map<String, String> toJson() {
    return <String, String>{ };
  }

  @override
  AccountModel createFromJson(Map<String, dynamic> data) {
    return AccountModel.fromJson(data);
  }

}