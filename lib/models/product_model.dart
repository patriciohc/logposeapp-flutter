class ProductModel {
  String id;
  String nombre;
  String descripcion;
  String codigo;
  double precio;
  double iva;
  List<String> imagenes;

  ProductModel.fromJson(Map<String, dynamic> data) {
    id = data['id'];
    nombre = data['nombre'];
    descripcion = data['descripcion'];
    precio = data['precio'] != null ? data['precio'].toDouble() : 0.0;
    codigo = data['codigo'];
    iva = data['iva'] != null ? data['iva'].toDouble() : 0.0;
    imagenes = new List<String>();
    if (data['imagenes'] != null) {
      data['imagenes'].forEach((item) => imagenes.add(item));
    }
  }

  get firstImage => imagenes.length > 0
      ? imagenes[0]
      : 'https://firebasestorage.googleapis.com/v0/b/logposeapp.appspot.com/o/app%2Fno-image445x267.jpg?alt=media';

  toJson() {
    return {'id': id};
  }
}
