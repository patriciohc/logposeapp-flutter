import 'package:logposeapp/models/base_http_model.dart';

class EmailModel extends BaseHttpModel<EmailModel> {
  String _correoElectronico;

  EmailModel(String email) {
    _correoElectronico = email;
  }

  EmailModel.fromJson(Map<String, dynamic> data) {
    _correoElectronico = data['correo_electronico'];
  }

  @override
  Map<String, String> toJson() {
    return <String, String>{
      'correo_electronico': _correoElectronico,
    };
  }

  @override
  EmailModel createFromJson(Map<String, dynamic> data) {
    return EmailModel.fromJson(data);
  }
}
