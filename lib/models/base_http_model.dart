import 'dart:convert';

abstract class BaseHttpModel<T> {

  String convertModelToJson() => json.encode(toJson());

  Map<String, dynamic> toJson();

  T createFromJson(Map<String, dynamic> data);

}