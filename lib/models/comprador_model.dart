
import 'package:logposeapp/models/sucursal_model.dart';

class CompradorModel {

  List<SucursalModel> favoritos;

  CompradorModel() {
    favoritos = [];
  }

  CompradorModel.formJson(Map data) {
    favoritos = [];
    List dataFavoritos = data['favoritos'] ?? [];
    dataFavoritos.forEach((element) {
      favoritos.add(SucursalModel.fromJson(element));
    });
  }

}