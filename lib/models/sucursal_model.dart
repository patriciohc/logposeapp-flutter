class SucursalModel {
  String id;
  String nombreEmpresa;
  String direccion;
  double lat;
  double lng;
  String telefono;
  String horaApertura;
  String horaCierre;
  String _logo;
  String posterPath;
  List<CategoriaProductos> categoriaProductos;
  String _promocionalImg;
  bool servicioDomicilio;
  bool hasSolicitudServicio;
  bool activo;

  SucursalModel.fromJson(Map<String, dynamic> data) {
    id = data['id'];
    activo = data['activo'] ?? false;
    nombreEmpresa = data['nombre_empresa'] ?? '';
    direccion = data['direccion'] ?? '';
    lat = data['lat'] ?? 0.0;
    lng = data['lng'] ?? 0.0;
    telefono = data['telefono'] ?? '';
    horaApertura = data['hora_apertura'] ?? '';
    horaCierre = data['hora_cierre'] ?? '';
    _logo = data['logo'] ?? '';
    servicioDomicilio = data['servicio_domicilio'];
    if (data['has_solicitud_servicio'] is bool) {
      hasSolicitudServicio = data['has_solicitud_servicio'];
    } else {
      hasSolicitudServicio = false;
    }

    categoriaProductos = [];
    if (data['categoria_productos'] != null) {
      data['categoria_productos'].forEach((item) {
        categoriaProductos.add(CategoriaProductos.fromJson(item));
      });
    }

    if (data['styles'] != null &&
        data['styles']['imagen_promocional'] != null) {
      _promocionalImg = data['styles']['imagen_promocional'];
    }
  }

  get promocionalImg => _promocionalImg != null ? _promocionalImg : '';
  get logo => _logo != null ? _logo : '';
}

class CategoriaProductos {
  String id;
  String nombre;
  String descripcion;

  CategoriaProductos.fromJson(Map data) {
    this.id = data['id'];
    this.nombre = data['nombre'];
    this.descripcion = data['descripcion'];
  }
}
