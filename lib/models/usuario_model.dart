
import 'package:flutter/material.dart';
import 'package:logposeapp/models/base_http_model.dart';
import 'package:logposeapp/utils/strings.dart';

class UsuarioModel extends BaseHttpModel<UsuarioModel> {

  String _id;
  String _nombre;
  String _apellidos;
  String _correoElectronico;
  String _telefono;
  String _password;

  Map<String, TextEditingController> _controller;

  UsuarioModel() {
    _controller = null;
  }

  UsuarioModel.fromJson(Map<String, dynamic> data) {
    _controller = null;
    _id = data['id'];
    _nombre = data['nombre'];
    _apellidos = data['apellidos'];
    _correoElectronico = data['correo_electronico'];
    _telefono = data['telefono'];
  }

  UsuarioModel.initController() {
    _controller = {
      'nombre': TextEditingController(),
      'apellidos': TextEditingController(),
      'correo_electronico': TextEditingController(),
      'telefono': TextEditingController(),
      'password': TextEditingController()
    };
  }

  get nombreCrtl => _controller['nombre'];
  get apellidosCrtl => _controller['apellidos'];
  get correoElectronicoCrtl => _controller['correo_electronico'];
  get telefonoCrtl => _controller['telefono'];
  get passwordCrtl => _controller['password'];

  get id => _id;
  get nombre => nombreCrtl != null ? capitalize(_controller['nombre'].text) : _nombre; 
  get apellidos => nombreCrtl != null ? capitalize(_controller['apellidos'].text) : _apellidos; 
  get correoElectronico => nombreCrtl != null ? _controller['correo_electronico'].text : _correoElectronico; 
  get telefono => nombreCrtl != null ? _controller['telefono'].text : _telefono; 
  get password => nombreCrtl != null ? _controller['password'].text : _password; 

  @override
  Map<String, String> toJson() {
    return <String, String>{
      'nombre': nombre,
      'apellidos': apellidos,
      'correo_electronico': correoElectronico,
      'telefono': telefono,
      'password': password
    };
  }

  @override
  UsuarioModel createFromJson(Map<String, dynamic> data) {
    return UsuarioModel.fromJson(data);
  }

}