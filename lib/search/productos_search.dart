import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_image/network.dart';
import 'package:logposeapp/blocs/shoping_cart/shoping_cart_bloc.dart';
import 'package:logposeapp/models/product_model.dart';
import 'package:logposeapp/models/sucursal_model.dart';
import 'package:logposeapp/providers/products_prodiver.dart';

class ProductosSearch extends SearchDelegate {
  final productsProvider = ProductsProvider();
  SucursalModel sucursal;
  String catSelected;

  ProductosSearch(this.sucursal, this.catSelected);

  @override
  List<Widget> buildActions(BuildContext context) {
    // Las acciones de nuestro AppBar
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Icono a la izquierda del AppBar
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return _buildSuggestions(context);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return _buildSuggestions(context);
  }

  Widget _buildSuggestions(BuildContext context) {
    // Son las sugerencias que aparecen cuando la persona escribe
    if (query.isEmpty) {
      return Container();
    }

    final size = MediaQuery.of(context).size;
    return FutureBuilder(
      future: productsProvider.getProducts(
        this.sucursal.id,
        texto: query,
        categoriaId: this.catSelected,
      ),
      builder:
          (BuildContext context, AsyncSnapshot<List<ProductModel>> snapshot) {
        if (snapshot.hasData) {
          final productos = snapshot.data;

          return ListView(
              children: productos.map((p) {
            return Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6.0),
              ),
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: FadeInImage(
                      image: NetworkImageWithRetry(p.firstImage),
                      placeholder: AssetImage('assets/img/loading.gif'),
                      fit: BoxFit.cover,
                    ),
                    title: Text(p.nombre),
                    subtitle: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: size.width - 200,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(p.descripcion),
                              if (sucursal.hasSolicitudServicio)
                                Text('\$' + p.precio.toString()),
                              if (!sucursal.hasSolicitudServicio)
                                Text('Precio no disponible')
                            ],
                          ),
                        ),
                        if (sucursal.hasSolicitudServicio)
                          IconButton(
                            color: Colors.red,
                            icon: Icon(Icons.add_shopping_cart),
                            onPressed: () {
                              BlocProvider.of<ShopingCartBloc>(context)
                                  .add(AddElementToCartEvent(p, sucursal));
                            },
                          )
                      ],
                    ),
                  ),
                ],
              ),
              //
            );
          }).toList());
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
